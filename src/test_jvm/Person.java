package test_jvm;
/**
 * 
test_jvm.Person object internals:
OFF  SZ               TYPE DESCRIPTION               VALUE
  0   8                    (object header: mark)     0x0000000000000001 (non-biasable; age: 0)
  8   4                    (object header: class)    0xf800f203
 12   4                int Person.i                  12324
 16   8               long Person.l                  1234
 24   1            boolean Person.has                true
 25   3                    (alignment/padding gap)   
 28   4   java.lang.String Person.aString            (object)
Instance size: 32 bytes
Space losses: 3 bytes internal + 0 bytes external = 3 bytes total
 *
 */
public class Person {
	
	public String aString = "asdfasdgdsfgdfs";  //引用类型，指针占用4个字节，字符串会存在其他的地方
	public int i = 12324; //占用4个字节
	public long l = 1234l;  //占用8个字节
	public boolean has = true; //占用1个字节
	
}
