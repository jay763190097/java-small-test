package test_jvm;

import org.openjdk.jol.info.ClassLayout;

public class TestObject {

	public static void main(String[] args) {
		Object name = new Object();
		System.out.println(ClassLayout.parseInstance(name).toPrintable());
		Person person = new Person();
		System.out.println(ClassLayout.parseInstance(person).toPrintable());
	}

}
