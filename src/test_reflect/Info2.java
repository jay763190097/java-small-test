package test_reflect;

import java.lang.reflect.Field;

public class Info2<T> {

	Class<?> entry;
	public Info2(Class<?> entry) {
		this.entry = entry;
	}

	public void printValue() {
		Field[] fields = entry.getClass().getFields();
		for (Field f : fields) {
			System.out.println(f.getName());
		}
	}
}
