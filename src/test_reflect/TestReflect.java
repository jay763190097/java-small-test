package test_reflect;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TestReflect {

	@Test
	public void test() {
		//可以得到字段名
		LSLSEntry entry = new LSLSEntry();
		printValue(entry);
	}
 
	@Test
	public void test1() {
		//得不到字段名，为什么？？？
		printValue(LSLSEntry.class);
	}
 
	@Test
	public void test2() {
		//得不到字段名，为什么？？
		PrintInfo2<LSLSEntry> info = new PrintInfo2<LSLSEntry>(LSLSEntry.class);
		info.printValue();
	}
 
	@Test
	public void test3() {
		//可以得到字段名, 用来得到entry对象的public属性的所有字段名
		LSLSEntry entry = new LSLSEntry();
		PrintInfo<LSLSEntry> info = new PrintInfo<LSLSEntry>(entry);
		info.printValue();
	}
 
	@Test
	public void test4() {
		//可以得到字段名
		Field[] fields = LSLSEntry.class.getFields();
		for (Field f : fields) {
			System.out.println(f.getName());
		}
	}
	
	
	private <T> void printValue(T entry) {
		Field[] fields = entry.getClass().getFields();
		for (Field f : fields) {
			System.out.println(f.getName());
		}
	}
	
	private class PrintInfo<T> {
		T entry;
		public PrintInfo(T entry) {
			this.entry = entry;
		}
 
		private void printValue() {
			Field[] fields = entry.getClass().getFields();
			for (Field f : fields) {
				System.out.println(f.getName());
			}
		}
	}
	
	
	private class PrintInfo2<T> {
		Class<?> entry;
		public PrintInfo2(Class<?> entry) {
			this.entry = entry;
		}
 
		private void printValue() {
			Field[] fields = entry.getClass().getFields();
			for (Field f : fields) {
				System.out.println(f.getName());
			}
		}
	}
	
}
