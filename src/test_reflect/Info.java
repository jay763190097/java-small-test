package test_reflect;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class Info<T> {

	//private String name;
	private T entry;
	
	public Info() {
		
	}
	
	public Info(T entry) {
		this.entry = entry;
	}
	
	public String printValue() {
		String fieldName = "";
		/*Field[] fields = getClass().getDeclaredFields();
		String clazzName = entry.getClass().getName();
		System.out.println(clazzName);*/
		
		Type t = this.getClass().getGenericSuperclass();
		System.out.println(t);
		if (ParameterizedType.class.isAssignableFrom(t.getClass())) {
			System.out.print("getActualTypeArguments:");
			for (Type t1 : ((ParameterizedType) t).getActualTypeArguments()) {
				System.out.print(t1 + ",");
			}
			System.out.println();
		}
		
		
		/*for (Field f : fields) {
			System.out.println(f.getName());
			System.out.println(f.getGenericType());
			System.out.println(f.getDeclaringClass().getClass().getName());
			System.out.println(f.getType().getTypeName());
			System.out.println(f.getClass().getName());
			if(clazzName.equals(f.getDeclaringClass().getName())) {
				fieldName = f.getName();
			}
		}*/
		return fieldName;
	}
	
	
	public void printValue2() {
		Class<?> t = getClass();
		if (ParameterizedType.class.isAssignableFrom(t)) {
			System.out.print("getActualTypeArguments:");
			for (Type t1 : ((ParameterizedType) (Type)t).getActualTypeArguments()) {
				System.out.print(t1 + ",");
			}
			System.out.println();
		}
	}

	public T getEntry() {
		return entry;
	}

	public void setEntry(T entry) {
		this.entry = entry;
	}
	
	public Class<T> getTClass()
    {
        Class<T> tClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return tClass;
    }


	/*public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}*/
	
	
	
}
