package test_reflect;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TestReflect2{

	@Test
	public void test() {
		Info<HashMap> info = new Info<HashMap>(new HashMap()){};
		info.printValue();
		/*Field[] fields = info.getClass().getDeclaredFields();
		for (Field field : fields) {
			System.out.println(field.getName());
			System.out.println(field.getType());
			System.out.println(field.getGenericType().getTypeName());
		}*/
	}
	
	
	@Test
	public void test2() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> clazz = Class.forName(Info.class.getName());
		//Field[] fields = clazz.getFields();
		Field[] fields = clazz.getDeclaredFields(); //可以获取到private属性名
		for (Field field : fields) {
			System.out.println(field.getName());
			System.out.println(field.getType());
			System.out.println(field.getDeclaringClass());
			System.out.println(field.getGenericType().getTypeName());
		}
		
		Info<Map> foo = new Info<Map>(){};
		Type type = foo.getClass().getGenericSuperclass();
		if(type instanceof Class) {
			System.out.println("yes");
		} else {
			System.out.println("no");
		}
		
	}
	
	
	@Test
	public void test3() {
		Info<Map> foo = new Info<Map>(){};  //匿名子类
        // 在类的外部这样获取
        Type type = ((ParameterizedType)foo.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        System.out.println(type);
        // 在类的内部这样获取
        System.out.println(foo.getTClass());
        
        Info<Map> f = new Info<Map>();
        Field[] fields = f.getClass().getDeclaredFields();
        String name = "";
        for (Field field : fields) {
        	String t = field.getGenericType().getTypeName();
        	if("T".equals(t)) {
        		name = field.getName();
        		break;
        	}
			//System.out.println(field.getType().getName());
			//System.out.println(field.getDeclaringClass());
		}
        
        System.out.println(name);
        
	}
	
}
