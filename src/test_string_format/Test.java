package test_string_format;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * String format练习  
 * %1$s
 * %s表示为字符串类型
 * 1$是被格式化的参数索引，索引值为1,代表第一个参数
 * @author Administrator
 *
 */
public class Test {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String pattern = "%1$s%2$tY%2$tm%2$td-%3$05d"; //ty输出两位 tY输出4位 05表示数字前面补0保证5位
		LocalDateTime today = LocalDateTime.now();
		//Date date = new Date();
		String result = String.format(pattern,"DI",today,192);
		System.out.println(result);
		
		/**
		 * java中split()特殊符号"." "|" "*" "\" "]"
		 * 
		 * 关于点的问题是用string.split("[.]") 解决。
		 * 关于竖线的问题用 string.split("\\|")解决。
		 *	关于星号的问题用 string.split("\\*")解决。
		 *	关于斜线的问题用 sring.split("\\\\")解决。
		 * 关于中括号的问题用 sring.split("\\[\\]")解决。
		 * 
		 */
	/*	String a = "A&B&AB";
		System.out.println(a.split("&").length);*/
		
		System.out.println("345436".equals(null));

	
	}

}
