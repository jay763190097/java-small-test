package test_sso;

import io.minio.MinioClient;
import io.minio.policy.PolicyType;

/**
 * bucket的名字需要遵守一定命名规范，字符中不能存在大写。详细请参见官方文档：
 * https://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html
 * @author jay
 *
 */
public class Test_Sso {

	public static void main(String[] args) throws Exception {
		
		//替换为实际地址ip
        String url = "http://192.168.0.8:9100/";
        String accessKey = "admin";
        String secretKey = "1185441306";
        //创建minio client
        MinioClient minioClient = new MinioClient(url,accessKey,secretKey);

        //String bucket = "minio/test-bucket";
        String bucket = "minio.test";
        String objectName = "testPic.jpeg";
        String file = "/home/jay/Desktop/984532661.jpeg";

        // 确保服务器时区对应
        boolean isExists = minioClient.bucketExists(bucket);
        if (!isExists) {
            //bucket桶不存在，则创建
            minioClient.makeBucket(bucket);
            /*
            设置bucket策略类型。前缀为*，策略只读，表示指定bucket下所有文件访问不需权限验证。
            请根据业务场景，谨慎设置！！！
            屏蔽该行代码，设置bucket下的文件对象默认需要权限验证。
            */
            minioClient.setBucketPolicy(bucket,"*", PolicyType.READ_ONLY);
        }

        //上传文件
        minioClient.putObject(bucket,objectName,file);

        //获取文件访问路径
        String fileObjPath = minioClient.getObjectUrl(bucket,objectName);
        System.out.println("access object full path is {}"+fileObjPath);

        //获取文件临时访问路径。有效时间单位为秒，不能大于7天。
        String presignedPath = minioClient.presignedGetObject(bucket,objectName,3600);
        System.out.println("access object temp path is {}"+presignedPath);
	}

}
