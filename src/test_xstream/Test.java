package test_xstream;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class Test {

	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, DocumentException {
		
		
		File file = new File("D:\\FI_VND_QEC_20190313135151_660.XML");
	/*	BufferedReader reader = new BufferedReader(new FileReader(file));
		String content = "";
		String tempString = null;
		int line = 1;
		//一次读一行，读入null时文件结束
		while ((tempString = reader.readLine()) != null) {
		//把当前行号显示出来
		//System.out.println("line " + line + ": " + tempString);
		content += tempString;
		line++;
		}
		reader.close();*/
		SAXReader reader = new SAXReader();
		Document document = reader.read(file);
		Element root = document.getRootElement();
		Element VENDOR = root.element("VENDOR");
		Element bankEle= VENDOR.element("item");
		Element dElement = bankEle.element("BANK_DETAIL");
		
		//System.out.println(dElement.asXML());
		
		//item 可以重复，所以这是无用的替换
		/*
		List<Element> items = dElement.elements("item");
		List<String> newitems = new ArrayList<>();
		items.stream().forEach(ele -> {
			Element newEle = dElement.addElement("detail_item");
			newEle.addText("@@@");
			List<Element> elements = ele.elements();
			StringBuilder sb = new StringBuilder();
			elements.stream().forEach(elnts -> {
				sb.append(elnts.asXML());
			});
			newitems.add(sb.toString());
			dElement.remove(ele);
		});
		
		String dEleStr = dElement.asXML().replaceAll("\r|\n| ", "");
		for (String string : newitems) {
			dEleStr = dEleStr.replaceFirst("@@@", string);
		}
		//System.out.println(dEleStr);
		
		
		Element newDet = bankEle.addElement("BANK_DETAIL");
		newDet.addText("$$");
		bankEle.remove(dElement);
		
		String bankEleStr = bankEle.asXML().replace("<BANK_DETAIL>$$</BANK_DETAIL>", dEleStr);
		bankEleStr = bankEleStr.replaceAll("\r|\n| ", "");
		
		System.out.println(bankEleStr); */
		
		//System.out.println(dElement.asXML().replaceAll("<detail_item>##</detail_item>", sb.toString()));
		//System.out.println(bankEle.asXML());
		//System.out.println(bankEle.asXML().replaceAll("$$", dElement.asXML().replaceAll("##", sb.toString())));
		
		XStream xstream = new XStream(new Dom4JDriver()) {
			protected MapperWrapper wrapMapper(MapperWrapper next) {
				return new MapperWrapper(next) {
		            @Override
		            public boolean shouldSerializeMember(Class definedIn, String fieldName) {
		            	//System.out.println(fieldName);
		            	// 不能识别的节点，掠过。
						if (definedIn == Object.class) {
							return false;
						}
						/*// 类无对应属性时掠过
						Field f = null;
						try {
							f = XmlVendorBankData.class.getDeclaredField(fieldName);
						} catch (Exception e) {
						}
						if (f == null) {
							return false;
						}*/
						return super
								.shouldSerializeMember(definedIn, fieldName);
		            }
		        };
		    }
		};
		
		//System.out.println(bankEle.asXML());
		
	    
	    String bankEleStr = "<item>\n" + 
	    		"	<LIFNR>1000000208</LIFNR>\n" + 
	    		"	<NAME1>上海冰天美帝企业管理有限公司</NAME1>\n" + 
	    		"	<NAME2 />\n" + 
	    		"	<NAME3 />\n" + 
	    		"	<NAME4 />\n" + 
	    		"	<BRSCH />\n" + 
	    		"	<SFRGR />\n" + 
	    		"	<STRAS>上海市上海市</STRAS>\n" + 
	    		"	<TELF1 />\n" + 
	    		"	<LOEVM />\n" + 
	    		"	<SPERR />\n" + 
	    		"	<BANK_DETAIL index=\"1\">\n" + 
	    		"		<name>名称</name>\n" + 
	    		"		<item>\n" + 
	    		"			<BANKS>CN</BANKS>\n" + 
	    		"			<BANKL>104290075097</BANKL>\n" + 
	    		"			<BANKN>454662503991</BANKN>\n" + 
	    		"			<BANKA>中国银行上海市江桥支行</BANKA>\n" + 
	    		"			<BVTYP />\n" + 
	    		"			<BKREF />\n" + 
	    		"			<KOINH>上海冰天美帝企业管理有限公司</KOINH>\n" + 
	    		"			<FLG_DELETE/>\n" + 
	    		"		</item>\n" + 
	    		"		<item>\n" + 
	    		"			<BANKS>CN</BANKS>\n" + 
	    		"			<BANKL>104290075097</BANKL>\n" + 
	    		"			<BANKN>454662503991</BANKN>\n" + 
	    		"			<BANKA>中国银行上海市江桥支行</BANKA>\n" + 
	    		"			<BVTYP />\n" + 
	    		"			<BKREF />\n" + 
	    		"			<KOINH>上海冰天美帝企业管理有限公司</KOINH>\n" + 
	    		"			<FLG_DELETE/>\n" + 
	    		"		</item>\n" + 
	    		"	</BANK_DETAIL>\n" + 
	    		"</item>";
	    //XStream xstream2 = new XStream(new DomDriver());//创建Xstram对象
	    //xstream2.autodetectAnnotations(true);
	    //xstream2.processAnnotations(XmlVendorBankData.class);
	    
		
		xstream.alias("item", XmlVendorBankData.class);
	    xstream.autodetectAnnotations(true);
	    //xstream.processAnnotations(XmlVendorBankData.class);
	    //xstream.processAnnotations(XmlVendorBankAcountData.class);
	    //xstream.aliasField("BANK_DETAIL", XmlVendorBankAcountData.class, "detail_item"); //属性别名
	    XmlVendorBankData bankData = (XmlVendorBankData) xstream.fromXML(bankEleStr);
	    System.out.println(bankData);
	    
	}
	
}
