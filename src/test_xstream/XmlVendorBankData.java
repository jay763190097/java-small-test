package test_xstream;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


/**
 * 供应商银行账户实体
 * @author 01375894
 *
 */
@SuppressWarnings("all")
@XStreamAlias("item")
public class XmlVendorBankData {
	/** 主键ID<系统自增长> */
	private Long id;
	private String LIFNR;// 供应商编码
	private String VENDOR_NAME;//供应商名称(NAME1和NAME2的合并)
	private String NAME1;// 供应商名称1
	private String NAME2;// 供应商名称2
	private String NAME3;// 供应商名称3
	private String NAME4;// 供应商名称4
	private String BRSCH;// 业务类型
	private String SAP_BRSCH;// sap业务类型
	private String STRAS;// 供应商地址
	private String TELF1;// 电话
	private String LOEVM;// 删除标识[X值为删除，否则不删除]
	private String SPERR;// 冻结标识[X值为冻结，否则不冻结]
	
	private String SFRGR; //供应商资质(VS01:4S店(属于一类维修厂) VS02:一类维修厂 VS03:二类维修厂)

	// 是否有效(1;有效 0:失效)
	private int VALID_FLG;
	// 修改时间
	private Date MODIFIED_TM;
	
	private Integer dataSource;
	
	/**
	 * @XStreamAlias("BANK_DETAIL")
	 * @XStreamImplicit(itemFieldName = "detail_item")
	 * 不能一次解决，必须定义一个detail对象，xml的数组是没有键的，不同于json
	 */
	//银行账户
	@XStreamAlias("BANK_DETAIL")
	private XmlBankDetail bankDetail;
	
	public String getLIFNR() {
		return LIFNR;
	}
	public void setLIFNR(String lIFNR) {
		LIFNR = lIFNR;
	}
	public String getNAME1() {
		return NAME1;
	}
	public void setNAME1(String nAME1) {
		NAME1 = nAME1;
	}
	public String getBRSCH() {
		return BRSCH;
	}
	public void setBRSCH(String bRSCH) {
		BRSCH = bRSCH;
	}
	public String getSTRAS() {
		return STRAS;
	}
	public void setSTRAS(String sTRAS) {
		STRAS = sTRAS;
	}
	public String getTELF1() {
		return TELF1;
	}
	public void setTELF1(String tELF1) {
		this.TELF1 = tELF1;
	}
	public String getLOEVM() {
		return LOEVM;
	}
	public void setLOEVM(String lOEVM) {
		LOEVM = lOEVM;
	}
	public String getSPERR() {
		return SPERR;
	}
	public void setSPERR(String sPERR) {
		SPERR = sPERR;
	}
	public String getNAME2() {
		return NAME2;
	}
	public void setNAME2(String nAME2) {
		NAME2 = nAME2;
	}
	public String getNAME3() {
		return NAME3;
	}
	public void setNAME3(String nAME3) {
		NAME3 = nAME3;
	}
	public String getNAME4() {
		return NAME4;
	}
	public void setNAME4(String nAME4) {
		NAME4 = nAME4;
	}
	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}
	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getVALID_FLG() {
		return VALID_FLG;
	}
	public void setVALID_FLG(int vALID_FLG) {
		VALID_FLG = vALID_FLG;
	}
	public Date getMODIFIED_TM() {
		return MODIFIED_TM;
	}
	public void setMODIFIED_TM(Date mODIFIED_TM) {
		MODIFIED_TM = mODIFIED_TM;
	}
	protected Long getId() {
		return id;
	}
	public String getSFRGR() {
		return SFRGR;
	}
	public void setSFRGR(String sFRGR) {
		SFRGR = sFRGR;
	}
	public Integer getDataSource() {
		return dataSource;
	}
	public void setDataSource(Integer dataSource) {
		this.dataSource = dataSource;
	}
	public String getSAP_BRSCH() {
		return SAP_BRSCH;
	}
	public void setSAP_BRSCH(String sAP_BRSCH) {
		SAP_BRSCH = sAP_BRSCH;
	}
	
	public XmlBankDetail getBankDetail() {
		return bankDetail;
	}
	public void setBankDetail(XmlBankDetail bankDetail) {
		this.bankDetail = bankDetail;
	}
	@Override
	public String toString() {
		return "XmlVendorBankData [id=" + id + ", LIFNR=" + LIFNR + ", VENDOR_NAME=" + VENDOR_NAME + ", NAME1=" + NAME1
				+ ", NAME2=" + NAME2 + ", NAME3=" + NAME3 + ", NAME4=" + NAME4 + ", BRSCH=" + BRSCH + ", SAP_BRSCH="
				+ SAP_BRSCH + ", STRAS=" + STRAS + ", TELF1=" + TELF1 + ", LOEVM=" + LOEVM + ", SPERR=" + SPERR
				+ ", SFRGR=" + SFRGR + ", VALID_FLG=" + VALID_FLG + ", MODIFIED_TM=" + MODIFIED_TM + ", dataSource="
				+ dataSource + ", bankDetail=" + bankDetail + "]";
	}
	
	
}
