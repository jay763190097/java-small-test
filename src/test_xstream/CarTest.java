package test_xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class CarTest {

	
	public static void main(String[] args) throws Exception {
	    String dataXml = "<car>"
	    		+ "<abc>sdf</abc>"
	    		+ "<bcd>1234</bcd>"
	    		+ "<carInfos dwName=\"sys.common.new_vehicle_DW\">\n" + 
	    		"    <carInfo index=\"1\">\n" + 
	    		"        <VehicleId>FTBAUD0088</VehicleId>\n" + 
	    		"        <VehicleName>福克斯CAF7163B5轿车</VehicleName>\n" + 
	    		"        <Remark>两厢 双离合 舒适型 国Ⅴ</Remark>\n" + 
	    		"        <VehiclePrice>101800</VehiclePrice>\n" + 
	    		"    </carInfo>\n" + 
	    		"    <carInfo index=\"2\">\n" + 
	    		"        <VehicleId>FTBAUD0078</VehicleId>\n" + 
	    		"        <VehicleName>福克斯CAF7163B5轿车</VehicleName>\n" + 
	    		"        <Remark>两厢 双离合 风尚型 国Ⅴ</Remark>\n" + 
	    		"        <VehiclePrice>113800</VehiclePrice>\n" + 
	    		"    </carInfo>\n" + 
	    		"    <carInfo index=\"3\">\n" + 
	    		"        <VehicleId>FTBAUD0097</VehicleId>\n" + 
	    		"        <VehicleName>福克斯CAF7163B5轿车</VehicleName>\n" + 
	    		"        <Remark>两厢 双离合 智行版 风尚型 国Ⅴ</Remark>\n" + 
	    		"        <VehiclePrice>115800</VehiclePrice>\n" + 
	    		"    </carInfo>\n" + 
	    		"</carInfos>" +
	    		"</car>\n";
	    XStream xstream = new XStream(new DomDriver());//创建Xstram对象
	    xstream.autodetectAnnotations(true);
	    xstream.processAnnotations(Car.class);
	    Car car = (Car) xstream.fromXML(dataXml);
	    //打印对象
	   /* System.out.printf("CarInfos dwName:%s\n", carInfos.getDwName());
	    for (CarInfo carInfo : carInfos.getCarInfoList()) {
	        System.out.printf("\tCarInfo index:%s\n", carInfo.getIndex());
	        System.out.printf("\tCarInfo VehicleId:%s\n", carInfo.getVehicleId());
	        System.out.printf("\tCarInfo VehicleName:%s\n", carInfo.getVehicleName());
	        System.out.printf("\tCarInfo VehiclePrice:%s\n", carInfo.getVehiclePrice());
	        System.out.printf("\tCarInfo Remark:%s\n", carInfo.getRemark());
	    }*/
	    //将对象转为xml，再次打印
	    String resultXml = xstream.toXML(car);
	    System.out.printf("=======================\n" + resultXml);
	}

	
}
