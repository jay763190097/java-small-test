package test_xstream;


import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class XmlVendorBankAcountData {

	//银行国家代码
	@XStreamAlias("BANKS")
	private String BANKS;
	//银行代码
	@XStreamAlias("BANKL")
	private String BANKL;
	//银行账号
	@XStreamAlias("BANKN")
	private String BANKN;
	//银行名称
	@XStreamAlias("BANKA")
	private String BANKA;
	//银行开户名称
	@XStreamAlias("KOINH")
	private String KOINH;
	
	@XStreamAlias("BVTYP")
	private String BVTYP;
	@XStreamAlias("BKREF")
	private String BKREF;
	@XStreamAlias("FLG_DELETE")
	private String FLG_DELETE;
	
	public String getBANKS() {
		return BANKS;
	}
	public void setBANKS(String bANKS) {
		BANKS = bANKS;
	}
	public String getBANKL() {
		return BANKL;
	}
	public void setBANKL(String bANKL) {
		BANKL = bANKL;
	}
	public String getBANKN() {
		return BANKN;
	}
	public void setBANKN(String bANKN) {
		BANKN = bANKN;
	}
	public String getBANKA() {
		return BANKA;
	}
	public void setBANKA(String bANKA) {
		BANKA = bANKA;
	}
	public String getKOINH() {
		return KOINH;
	}
	public void setKOINH(String kOINH) {
		KOINH = kOINH;
	}
	
	public String getBVTYP() {
		return BVTYP;
	}
	public void setBVTYP(String bVTYP) {
		BVTYP = bVTYP;
	}
	
	public String getBKREF() {
		return BKREF;
	}
	public void setBKREF(String bKREF) {
		BKREF = bKREF;
	}
	
	public String getFLG_DELETE() {
		return FLG_DELETE;
	}
	public void setFLG_DELETE(String fLG_DELETE) {
		FLG_DELETE = fLG_DELETE;
	}
	@Override
	public String toString() {
		return "XmlVendorBankAcountData [BANKS=" + BANKS + ", BANKL=" + BANKL + ", BANKN=" + BANKN + ", BANKA=" + BANKA
				+ ", KOINH=" + KOINH + "]";
	}
}
