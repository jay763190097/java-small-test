package test_xstream;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("car")
public class Car {
	
	@XStreamAsAttribute
	private String abc;
	
	@XStreamAlias("bcd")
	private Integer bcd;
	
	@XStreamAlias("carInfo")
	private CarInfos carInfos;
	
	public String getAbc() {
		return abc;
	}

	public void setAbc(String abc) {
		this.abc = abc;
	}

	public CarInfos getCarInfos() {
		return carInfos;
	}

	public void setCarInfos(CarInfos carInfos) {
		this.carInfos = carInfos;
	}

	public Integer getBcd() {
		return bcd;
	}

	public void setBcd(Integer bcd) {
		this.bcd = bcd;
	}
}
