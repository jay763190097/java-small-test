package test_xstream;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("carInfo")//对应carInfo元素
public class CarInfo {

    @XStreamAsAttribute
    private String index;//对应carInfo的index属性

    @XStreamAlias("VehicleId")
    private String vehicleId;//对应carInfo的VehicleId子元素

    @XStreamAlias("VehicleName")
    private String VehicleName;//对应carInfo的VehicleName子元素

    @XStreamAlias("Remark")
    private String remark;//对应carInfo的Remark子元素

    @XStreamAlias("VehiclePrice")
    private String vehiclePrice;//对应carInfo的VehiclePrice子元素
    
   

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleName() {
		return VehicleName;
	}

	public void setVehicleName(String vehicleName) {
		VehicleName = vehicleName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getVehiclePrice() {
		return vehiclePrice;
	}

	public void setVehiclePrice(String vehiclePrice) {
		this.vehiclePrice = vehiclePrice;
	}

}

