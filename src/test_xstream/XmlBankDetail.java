package test_xstream;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("BANK_DETAIL")
public class XmlBankDetail {
	
	@XStreamAsAttribute
	private Integer index;
	
	@XStreamAlias("name")
	private String name;
	
	@XStreamImplicit(itemFieldName = "item")
	private List<XmlVendorBankAcountData> acountDatas = new ArrayList<>();
	
	public List<XmlVendorBankAcountData> getAcountDatas() {
		return acountDatas;
	}
	public void setAcountDatas(List<XmlVendorBankAcountData> acountDatas) {
		this.acountDatas = acountDatas;
	}
	
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "XmlBankDetail [index=" + index + ", name=" + name + ", acountDatas=" + acountDatas + "]";
	}
}
