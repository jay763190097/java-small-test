package test_object_extends;

import java.util.HashMap;

/**
 * <h3>java类继承相关测试</h3>
 * <table align="center">
 * <tr><td></td>       <td>同一个类</td><td>同一个包</td><td>不同包的子类</td><td>不同包的非子类</td></tr>
 * <tr><td>Private</td>	 <td>√</td>     <td></td>        <td></td>          <td></td> </tr>
 * <tr><td>Default</td>	 <td>√</td>	    <td>√</td>		 <td></td>			<td></td> </tr>
 * <tr><td>Protected</td><td>√</td>	    <td>√</td>		 <td>√</td>   		<td></td> </tr>
 * <tr><td>Public</td>	 <td>√</td>	    <td>√</td>		 <td>√</td>			<td>√</td></tr>
 * <table>
 *	public： Java语言中访问限制最宽的修饰符，一般称之为“公共的”。被其修饰的类、属性以及方法不<br>
 *	　　　　　仅可以跨类访问，而且允许跨包（package）访问。<br>
 *	private: Java语言中对访问权限限制的最窄的修饰符，一般称之为“私有的”。被其修饰的类、属性以<br>
 *	　　　　　及方法只能被该类的对象访问，其子类不能访问，更不能允许跨包访问。<br>
 *	protect: 介于public 和 private 之间的一种访问修饰符，一般称之为“保护形”。被其修饰的类、<br>
 *	　　　　　属性以及方法只能被类本身的方法及子类访问，即使子类在不同的包中也可以访问。<br>
 *	default：即不加任何访问修饰符，通常称为“默认访问模式“。该模式下，只允许在同一个包中进行访<br>
 *	　　　　　问。
 */
public class Test {
	
	public HashMap<String,Parents> map = new HashMap<>();
	
	public void addpeople(Parents parents){
		map.put("adc", parents);
	}
	
	public static void main(String[] args) {
		
		Test test = new Test();
		
		Sun1 sun1 = new Sun1();
		sun1.setName("zhangsan");
		sun1.setAge(24);
		sun1.setHoby("234");
		test.addpeople(sun1);  //申明接收父类型，可以给子类型
		
		Sun1 sun11 = (Sun1) test.map.get("adc");   //子类型接收父类型（上面实际上保存的就是子类型Sun1）
		System.out.println(sun11.getHoby());
		System.out.println(sun11.getName());
		System.out.println(sun11.getAge());
		
		
		Parents parents = new Sun2();
		parents.setAge(25);
		parents.setName("jay");
		test.addpeople(parents);  //申明接收父类型，给父类型，实际的对象时 Sun2
		
		Sun2 sun12 = (Sun2)test.map.get("adc");  //转为new 的子类型
		System.out.println(sun12.getName());
		System.out.println(sun12.getAge());
		
		
		/**
		 * 能否强转  1、父类可以接收所有子类
		 *        2、父类能否强转为子类，只和和对象的实际类型有关（new 的时候用的类型，和接收类型无关）。如果实际类型不是它，是它的父类或者兄弟类，都不可强转
		 */
		
		//练习hashCode
		String string = "254sdfgfdhfghfdgh3dfhfdh";
		System.out.println(string.hashCode());   
	}
}
