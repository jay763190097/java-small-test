package test_object_extends;

public abstract class Parents {
	
	protected String name;  // protected 允许子类使用该属性
	private int age;        // private 子类不能使用此属性
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
}
