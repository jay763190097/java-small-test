package test_hbase;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class VmapOtherRestServiceImpl {

    private Logger logger = LoggerFactory.getLogger(VmapOtherRestServiceImpl.class);

    @Autowired
    private Map<String, Object> redisUtil = new HashMap<>();

    public void queryHbase(Object param) {
        Table table = null;
        try {
            Integer type = 1;
            String imei = "";
            if(type == null || StringUtils.isEmpty(imei)) {
            }
            Configuration configuration = HBaseConfiguration.create();
            Connection connection = ConnectionFactory.createConnection(configuration);
            String rt = "";
            List<Object> list = new ArrayList<>();
            // 查询里程信息
            if (type == 1) {
                String tableName = "";
                String key = tableName + "#" + imei;
                Object totalMile = (Object) redisUtil.get(key);
                if(totalMile == null) {
                    table = connection.getTable(TableName.valueOf(tableName));
                    scanDataByRange(list, table, imei, type, key);
                    //scanDataFilter(list, table, imei, type);
                } else {
                    list.add(totalMile);
                }
                if(CollectionUtils.isEmpty(list)) {
                    redisUtil.put(key, null);
                }
            }
            // 查询位置信息
            else if (type == 2) {
                String tableName = "";
                String key = tableName + "#" + imei;
                Object position = (Object) redisUtil.get(key);
                if(position == null) {
                    table = connection.getTable(TableName.valueOf(tableName));
                    scanDataByRange(list, table, imei, type, key);
                    //scanDataFilter(list, table, imei, type);
                } else {
                    list.add(position);
                }
                if(CollectionUtils.isEmpty(list)) {
                    redisUtil.put(key, null);
                }
            }
        } catch (Exception e) {
            if(table != null) {
                try {
                    table.close();
                } catch (IOException ex) {
                    logger.error("close table error", e);
                }
            }
            logger.error("query error", e);
        }
    }


    /**
     * Filter方式
     * 扫描里程和位置数据
     * @param table
     * @param value
     * @param type 1-里程 2-位置
     * @return
     * @throws IOException
     */
    public void scanDataFilter(List<Object> list, Table table, String value, Integer type) throws IOException {
        int n = 2;
        //默认两个预分区
        String startKey = (n % 2 == 0 ? "00": "01") + '-' + value+"-";
        Scan scan = new Scan();
        scan.setReversed(true); //反序
        Filter pageFilter = new PageFilter(1); //只返回最新的一条数据
        Filter prefixfilter = new PrefixFilter(Bytes.toBytes(startKey));
        scan.setFilter(new FilterList(Arrays.asList(pageFilter, prefixfilter)));
        ResultScanner resultScanner = table.getScanner(scan);
        for (Result result : resultScanner) {
            if(type == 1) {
            } else if(type == 2) {
            }
        }
    }



    /**
     * 扫描里程数据
     * startRow-stopRow 非全表扫描
     * @param table
     * @param value
     * @param type 1-里程 2-位置
     * @return
     * @throws IOException
     */
    public void scanDataByRange(List<Object> list, Table table, String value, Integer type, String key) throws IOException {
        Integer maxDay = 3;
        Integer perHour = 2;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, - (maxDay * perHour * 2));
        //允许的最小时间
        String minDate = DateFormatUtils.format(calendar, "yyyyMMddHHmmss");
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, -perHour);
        //第一次查询开始时间
        String curdate = DateFormatUtils.format(calendar, "yyyyMMddHHmmss");
        Scan scan = new Scan();
        int n = 1;
        //默认两个预分区
        String startKey = (n % 2 == 0 ? "00": "01") + '-' + value+"-";
        scan.setStopRow(Bytes.toBytes(startKey+curdate));
        scan.setStartRow(Bytes.toBytes(startKey+DateFormatUtils.format(date,"yyyyMMddHHmmss")));
        scan.setReversed(true);
        ResultScanner resultScanner = table.getScanner(scan);
        Iterator<Result> iterator = resultScanner.iterator();
        while((!iterator.hasNext()) && curdate.compareTo(minDate) >= 0) {
            calendar.add(Calendar.HOUR_OF_DAY, -perHour);
            curdate = DateFormatUtils.format(calendar, "yyyyMMddHHmmss");
            scan.setStopRow(Bytes.toBytes(startKey+curdate));
            iterator = table.getScanner(scan).iterator();
        }
        int i = 0;
        while (iterator.hasNext() && i < 1) {
            if(type == 1) {
            } else if(type == 2) {
            }
            i++;
        }
    }

}
