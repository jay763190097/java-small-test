package test_collection_sort;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(Level.上级医师);
		
		//java8 map.merge
		Map<String, Integer> map = new HashMap<>();
		map.put("A", 1);
		map.put("B", 2);
		//map.put("C", 5);
		//map.merge("A", null, (o,n) -> n == null ? 0 : o+n);
		//System.out.println(map.get("A"));
		
		//改变存在的值
	/*	System.out.println(map.compute("A", (o,n) -> n == null ? 42 : n+41));
		System.out.println(map);*/
		//增加一个新值
		/*System.out.println(map.compute("X", (o,n) -> o == null ? 42 + 10 : n+41));
		System.out.println(map);*/
		
		/*map.put("X",1);
		System.out.println(map.merge("X",1,(v1, v2) -> null)); //移除x属性
		System.out.println(map);*/
		
		//加入之前没有值的话，相当于put("C",20) 后面的表达式直接不执行  n * 5 应该放在20的位置
		/*map.merge("C", 20*5, (o,n) -> o == null ? o :  n + o);
		map.merge("E", 20*5, (o,n) -> n == null ? o :  n + o);
		map.merge("F", 20*5, (o,n) -> n + o);
		System.out.println(map);
		
		
		map.compute("K", (n,o) -> o == null ? 0 : o + 1);
		System.out.println(map.get("K"));
		map.compute("K", (n,o) -> o == null ? 0 : o + 1);
		System.out.println(map.get("K"));
		
		Map<String, Integer> mp = new HashMap<>();
		mp.put("xxx", 3);
		//int b = mp.get("zzz"); //声名的是Integer用int接收，如果值为空会报空指针
		int a = Optional.ofNullable(mp.get("xxx")).orElse(1); 
		System.out.println(a);*/
		
		
		
		/*Student student = new Student();
		student.setName("11");
		student.setId(1);
		Student student2 = new Student();
		student2.setName("22");
		student2.setId(1);
		List<Student> students = new ArrayList<>();
		students.add(student);
		students.add(student2);
		Map<Integer, Student> stuMap = new HashMap<>();
		for (Student student3 : students) {
			stuMap.compute(student3.getId(), (n,o) -> {
				if(o == null) {
					return student3;
				} else {
					o.setName(o.getName()+student3.getName());
					return o;
				}
			});
		}
		
		for (Map.Entry<Integer, Student> entry : stuMap.entrySet()) {
			System.out.println(entry.getKey());
			System.out.println(entry.getValue());
		}*/
		
		
		//map 聚合 合并
		Map<Long, Integer> map11 = new HashMap<>();
		Map<Long, Integer> map22 = new HashMap<>();
		map11.put(11L, 5);
		map11.put(12L, 5);
		map11.put(13L, 5);
		map22.put(11L, 6);
		map22.put(13L, 15);
		map11 = Stream.concat(map11.entrySet().stream(), map22.entrySet().stream())
		  .collect(Collectors.toMap(
		    Map.Entry::getKey, 
		    Map.Entry::getValue,
		    (v1, v2) -> v1 + v2));
		System.out.println(map11);
		
		
		
		
		
		
		
		
	}

}
