package test_collection_sort;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TestListSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student student = new Student();
		student.setId(1);
		student.setName("小明");
		student.setBirthday(new Date(12000));
		
		Student student2 = new Student();
		student2.setId(2);
		student2.setName("小红");
		student2.setBirthday(new Date(10000));
		
		List<Student> students = new ArrayList<>();
		students.add(student);
		students.add(student2);
		
		//升序
		students.sort(Comparator.comparing(Student::getBirthday));
		//倒序
		//students.sort(Comparator.comparing(Student::getBirthday).reversed());
		
		
		for (Student student3 : students) {
			System.out.println(student3.toString());
		}
	}

}
