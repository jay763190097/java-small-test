package test_SPI;

/**
 * 封装为jar包
 * @author 01375894
 *
 */
public interface Driver {
	
	String connect();
	
}
