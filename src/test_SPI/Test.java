package test_SPI;

import java.util.ServiceLoader;

/**
 * 在resources/META-INF/services目录下创建一个以解耦全路径命名的文件，在里面填写实现类拓展
 * 使用ServiceLoader加载所有的拓展
 * @author 01375894
 *
 */
public class Test {

	public static void main(String[] args) {
		ServiceLoader<Driver> serviceLoader = ServiceLoader.load(Driver.class);
		serviceLoader.forEach(driver -> System.out.println(driver.connect()));
	}

}
