package test_utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {
	
	private DateUtils(){
		
	}
	
	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);


    // 格式：年－月－日 小时：分钟：秒
    public static final String FORMAT_ONE = "yyyy-MM-dd HH:mm:ss";
    // 格式：年－月－日 小时：分钟
    public static final String FORMAT_TWO = "yyyy-MM-dd HH:mm";
    // 格式：年月日 小时分钟秒
    public static final String FORMAT_THREE = "yyyyMMdd-HHmmss";
    // 格式：年月日小时分钟秒
    public static final String FORMAT_FOUT = "yyyyMMddHHmmss";
    // 格式：年月日小时分钟秒毫秒数
    public static final String FORMAT_FIVE = "yyyyMMddHHmmssSSS";
    // 格式：年－月－日
    public static final String LONG_DATE_FORMAT = "yyyy-MM-dd";
    // 格式：月－日
    public static final String SHORT_DATE_FORMAT = "MM-dd";
    
    public static final String FORMAT_SIX = "HHmm";
    
    public static final String FORMAT_HH_MM = "HH:mm";

    
    public static final String FORMAT_SEVEN = "yyyyMMdd";

    // 会计期格式
    public static final String PERIOD_DATE_FORMAT = "yyyyMM";
    
    public static final String END_TIME = " 23:59:59";
    
    public static final String BEGIN_TIME = " 00:00:00";
    
    /**
     * 获取指定日期N个小时后的时间
     * @return
     */
    public static String getBeforeHourTimeOfAppointTime(Date date , int hour) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + hour);  
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_ONE);
        return df.format(calendar.getTime());
    }
    
    /**
     * 获取指定日期N个小时后的时间
     * @return
     */
    public static Date getBeforeHourDateOfAppointTime(Date date , int hour) {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + hour);  
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_ONE);
        return calendar.getTime();
    }
    

    public static String nowDateString() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_ONE);
        return dateFormat.format(date);
    }

    public static String getPreDayEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(LONG_DATE_FORMAT);
        return dateFormat.format(date) + END_TIME;
    }
    
    public static Date getDateByTimes(String str,String format_time) {
    	 SimpleDateFormat format = new SimpleDateFormat(FORMAT_ONE);// 24小时制
         Date date = null;
         try {
             date = format.parse(str+format_time);
         } catch (Exception ex) {
         	logger.error("",ex);
         }
    	return date ;
    }

    public static String getPreDayStartDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date date = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(LONG_DATE_FORMAT);
        return dateFormat.format(date) + BEGIN_TIME;
    }

    public static String todayStartDateString() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(LONG_DATE_FORMAT);
        return dateFormat.format(date) + BEGIN_TIME;
    }

    public static String todayEndDateString() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(LONG_DATE_FORMAT);
        return dateFormat.format(date) + END_TIME;
    }

    public static String nowDateYmd() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(LONG_DATE_FORMAT);
        return dateFormat.format(date);
    }

    public static String getFirstDay() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(LONG_DATE_FORMAT);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
        String first = dateFormat.format(c.getTime()) + BEGIN_TIME;
        return first;
    }

    /**
     * 当月最后一天
     * 
     * @return
     */
    public static String getLastDay() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(LONG_DATE_FORMAT);
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
        String last = dateFormat.format(ca.getTime()) + END_TIME;
        return last;

    }

    /**
     * 把符合日期格式的字符串转换为日期类型
     */
    public static java.util.Date stringtoDate(final String dateStr, String format) {
        Date d;
        if (StringUtils.isEmpty(format)) {
            format = FORMAT_ONE;
        }
        SimpleDateFormat formater = new SimpleDateFormat(format);
        try {
            // formater.setLenient(false);
            d = formater.parse(dateStr);
        } catch (Exception e) {
            logger.error("",e);
            d = null;
        }
        return d;
    }

    /**
     * 获取当前时间的指定格式
     */
    public static String getCurrDate() {
        return getCurrDate(null);
    }

    /**
     * 获取当前时间的指定格式
     */
    public static String getCurrDate(final String format) {
        return dateToString(new Date(), format);
    }

    /**
     * 把日期转换为字符串
     */
    public static String dateToString(final java.util.Date date, String format) {
        String result = "";
        if (StringUtils.isEmpty(format)) {
            format = FORMAT_ONE;
        }
        if (date == null) {
            return result;
        }
        SimpleDateFormat formater = new SimpleDateFormat(format);
        try {
            result = formater.format(date);
        } catch (Exception e) {
            logger.error("",e);
        }
        return result;
    }

    /**
     * 获取某年某月的天数
     */
    public static int getDaysOfMonth(final int year, final int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获得当前月份
     */
    public static int getToMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 获得当前年份
     */
    public static int getToYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 获得当前月份第一天日期
     */
    public static String getFirstByCurrMoth() {
        String currDateStr = getCurrDate(LONG_DATE_FORMAT);
        return currDateStr.substring(0, 8) + "01";
    }

    /**
     * 获得当前月份最后一天的日期
     */
    public static String getLastByCurrMoth() {
        String currDateStr = getCurrDate(LONG_DATE_FORMAT);
        return currDateStr.substring(0, 8) + getDaysOfMonth(getToYear(), getToMonth());
    }

    public static boolean isTimeOut(String datestr, int h) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_ONE);// 24小时制
        Date date = null;
        try {
            date = format.parse(datestr);
        } catch (Exception ex) {
        	logger.error("",ex);
        }
        if (date == null) {
            return false;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, h);// 24小时制
        // cal.add(Calendar.HOUR, x);12小时制
        date = cal.getTime();
        Date now = new Date();

        return date.before(now);
    }

    public static boolean isTimeOutM(String datestr, int m) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_ONE);// 24小时制
        Date date = null;
        try {
            date = format.parse(datestr);
        } catch (Exception ex) {
        	logger.error("",ex);
        }
        if (date == null){
        	return false;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, m);
        date = cal.getTime();
        Date now = new Date();

        return date.before(now);
    }

    /**
     * 得到一个间隔时间,比如,要获得当前时间90s后的时间,得到指定时间1天之后的时间
     * 
     * @param date 指定时间
     * @param type 和Calendar中的时间类型保持一致
     * @param mount 增加或减少的时间
     * @return
     */
    public static Date getIntervalDate(Date date, int type, int amount) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        calendar.add(type, amount);
        return calendar.getTime();

    }

    /**
     * 取得格式化的服务器时间
     * 
     * @param timeFormat 时间：yyyyMMddHHmmss；日期：yyyyMMdd
     * @return String
     */
    public static String getFormatTime(final String format, final Date date) {
        final SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }

    /**
     * 将String类型的日期格式为Date类型
     * 
     * @param date
     * @return Date类型
     */
    public static Date strToDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_ONE);// 24小时制
        java.util.Date time = null;
        try {
            time = sdf.parse(date);
        } catch (ParseException e) {
        	logger.error("",e);
        }
        return time;
    }

    /**
     * 获取当前时间
     * 
     * @return 返回 String类型的时间
     */
    public static String getCurrentTime() {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_ONE);
        String currentStrDate = dateFormat.format(now);
        return currentStrDate;
    }

    /**
     * 两个时间进行比较
     * 
     * @param prefixStrTime 前缀时间
     * @param suffixStrTime 后缀时间
     * @return true:大于；false:小于；
     */
    public static boolean compareToTime(String prefixStrTime, String suffixStrTime) {
        Calendar prefixCalendar = Calendar.getInstance();
        Calendar suffixCalendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_ONE);
        try {
            prefixCalendar.setTime(dateFormat.parse(prefixStrTime));
            suffixCalendar.setTime(dateFormat.parse(suffixStrTime));
        } catch (ParseException e) {
        	logger.error("",e);
        }
        int result = prefixCalendar.compareTo(suffixCalendar);
        return result >= 0 ? true : false;
    }

    /**
     * 与前时间比较
     * 
     * @param strTime
     * @return 大:true, 小：false;
     */
    public static boolean compareCurrentTime(Date date) {
        String currentTime = getCurrentTime();
        String time = getFormatTime(FORMAT_ONE, date);
        return compareToTime(currentTime, time);
    }

    /**
     * 将时间戳转化为Date
     */
    public static Date getStartTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String returnDate = format.format(date);
        return strToDate(returnDate);
    }

    /**
     *
     */
    public static Date getStartTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String returnDate = format.format(new Date());
        return strToDate(returnDate);
    }
    
    /**
     * 比较两个Date是否是同一天
     * @param args
     */
    public static boolean isTwoDateTheSameDay(Date dateFirst, Date dateSecond) {
        if(null == dateFirst) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(dateFirst);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(dateSecond);

        boolean isSameYear = cal1.get(Calendar.YEAR) == cal2
                .get(Calendar.YEAR);
        boolean isSameMonth = isSameYear
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
        boolean isSameDate = isSameMonth
                && cal1.get(Calendar.DAY_OF_MONTH) == cal2
                        .get(Calendar.DAY_OF_MONTH);

        return isSameDate;
    }

    /*public static void main(String[] args) {

        Calendar validTime = Calendar.getInstance();
        validTime.add(Calendar.DAY_OF_MONTH, -10);
        validTime.set(Calendar.HOUR_OF_DAY, 23);
        validTime.set(Calendar.MINUTE, 59);
        validTime.set(Calendar.SECOND, 59);
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("time:"+format.format(validTime.getTime())+",time2:"+validTime.getTimeInMillis());
        
        
        
    }*/

    /*public static boolean isInStoreAngle(double firstOrderAngle, double orderAngle, double storeAngle) {
        double minAngle = firstOrderAngle - storeAngle;
        double maxAngle = firstOrderAngle + storeAngle;
        if (minAngle >= 0 && maxAngle <= 360) {
            // 在一定角度范围内
            if (orderAngle >= minAngle && orderAngle <= maxAngle) {
                return true;
            }
        } else if (minAngle >= 0 && maxAngle >= 360) {
            // 在一定角度范围内
            if ((orderAngle >= minAngle && orderAngle <= 360) || (orderAngle >= 0 && orderAngle <= (maxAngle - 360))) {
                return true;
            }
        } else if (minAngle < 0 && maxAngle <= 360) {
            if ((orderAngle >= (360 + minAngle) && orderAngle <= 360) || (orderAngle >= 0 && orderAngle <= maxAngle)) {
                return true;
            }
        } else {
            //这种情况即minAngle < 0 && maxAngle >= 360 正常配置下，是不可能出现这种情况
            //因为storeAngle最大为90度，所以就不存在minAngle < 0 而 maxAngle >= 360的情况。
            return false;
        }

        return false;
    }*/

    /**
     * 判断日期是否是在10天内
     * 
     * @param args
     */
   /* public static boolean isInDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String paramDate = format.format(date);
        String nowDate = format.format(new Date());
        if (paramDate.equals(nowDate)) {
            return true;
        }
        Calendar startTime = Calendar.getInstance();
        startTime.add(Calendar.DAY_OF_MONTH, -10);
        startTime.set(Calendar.HOUR_OF_DAY, 23);
        startTime.set(Calendar.MINUTE, 59);
        startTime.set(Calendar.SECOND, 59);
        if(date.after(startTime.getTime())) {
            return true;
        }
        return false;
    }*/

    /*
     * public static void main(String[] args) { Calendar c = Calendar.getInstance();
     * c.set(Calendar.HOUR_OF_DAY, 0); c.set(Calendar.MINUTE, 0); c.set(Calendar.SECOND, 0);
     * 
     * System.out.println("" + c.getTime()); }
     */

    /**
     * 获取开始时间和接受时间 (今天凌晨到当前时间)
     * 
     * @return
     */
    public static Map<String, Object> getStartAndEndTiem(Date date) {
        Map<String, Object> map = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        map.put("endTime", calendar.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        map.put("startTime", calendar.getTime());
        return map;
    }

    /**
     * 获取开始时间和接受时间 (今天凌晨到当前时间)
     * 
     * @return
     */
    public static Map<String, Object> getStartAndEndTiemByOneDay(Date date) {
        Map<String, Object> map = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        map.put("startTime", calendar.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        map.put("endTime", calendar.getTime());
        return map;
    }
    
    /**
     * 获取当前星期几
     * @param dt
     * @return
     */
    public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"7", "1", "2", "3", "4", "5", "6"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0){
        	w = 0;
        }
        return weekDays[w];
    }
    
    /**
     * 获取今天凌晨0点时间
     * 
     * @return
     */
	public static Date getCurrentDateStart() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
    
	/**
	 * 获取明天凌晨0点时间
	 * @return
	 */
	public static Date getTomorrowDateStart() {
		return plusDays(getCurrentDateStart(), 1);
	}
	
    /**
	 * 计算指定日期按日增加（或减少）后的日期。<br/>
	 * 
	 * <pre>
	 * plusDays(date(2016-08-11), 1)  --> 2016-08-12
	 * plusDays(date(2016-08-11), -1)  --> 2016-08-11
	 * </pre>
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date plusDays(Date date, int days){
		Calendar calendar = Calendar.getInstance(); //得到日历
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		return calendar.getTime();
	}
}
