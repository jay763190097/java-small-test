package test_utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.Lists;

/**
 * 分批执行工具类
 *
 * @author weiqiang_tan
 * @date 2019/3/19 10:37
 */
public class BatchesUtils {

    public static final ThreadLocal<List> threadLocal = new ThreadLocal<>();

    /**
     * 分批次执行方法，list为空则不执行
     *
     * @param batchSize 批次大小
     * @param list      需分批的list
     * @param supplier  待执行方法
     * @param <T>       待执行方法的返回类型
     * @return
     */
    public static <T> List<T> batchExe(int batchSize, List list, Supplier<T> supplier) {
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }

        List<T> result = new ArrayList<>();
        int total = list.size();

        for (int i = 0; i < total; i += batchSize) {
            int nextIndex = i + batchSize;
            List subList = list.subList(i, Math.min(total, nextIndex));
            threadLocal.set(subList);
            T t = supplier.get();
            result.add(t);
            threadLocal.remove();
        }

        return result;
    }

    /**
     * 分批次执行方法，list为空则不执行
     *
     * @param batchSize 批次大小
     * @param list      需分批的list
     * @param consumer  待执行方法
     * @return
     */
    public static <T> void batchExe2(int batchSize, List<T> list, Consumer<List<T>> consumer) {
        if (CollectionUtils.isEmpty(list)) {
            return ;
        }

        int total = list.size();

        for (int i = 0; i < total; i += batchSize) {
            int nextIndex = i + batchSize;
            List<T> subList = list.subList(i, Math.min(total, nextIndex));
            consumer.accept(subList);
        }
    }


    /**
     * 分批次执行方法，list为空则不执行,批次执行失败可以中断
     * @param batchSize
     * @param list
     * @param consumer
     * @param <T>
     */
    public static <T> void batchExeCanBreak(int batchSize, List<T> list, Consumer<Map<String, Object>> consumer) {
        if (CollectionUtils.isEmpty(list)) {
            return ;
        }
        int total = list.size();
        for (int i = 0; i < total; i += batchSize) {
            int nextIndex = i + batchSize;
            List<T> subList = list.subList(i, Math.min(total, nextIndex));
            try {
                Map<String, Object> map = new HashMap<>(1);
                map.put("index", i);
                map.put("list", subList);
                consumer.accept(map);
            } catch (RuntimeException e) {
                break;
            }
        }
    }


    /**
     * 分批次执行方法，
     * list为空时，由调用者自行在方法中决定是否执行
     *
     * @param batchSize 批次大小
     * @param list      需分批的list
     * @param supplier  待执行方法
     * @param <T>       待执行方法的返回类型
     * @return
     */
    public static <T> List<T> batchExeAtLeastOne(int batchSize, List list, Supplier<T> supplier) {
        List<T> result = new ArrayList<>();

        int total;
        if (CollectionUtils.isEmpty(list)) {
            total = 1;
        } else {
            total = list.size();
        }

        for (int i = 0; i < total; i += batchSize) {
            int nextIndex = i + batchSize;
            if (CollectionUtils.isEmpty(list)) {
                threadLocal.set(Lists.newArrayList());
            } else {
                List subList = list.subList(i, Math.min(total, nextIndex));
                threadLocal.set(subList);
            }
            T t = supplier.get();
            result.add(t);
            threadLocal.remove();
        }

        return result;
    }

    /**
     * 异步分批次执行方法，list为空则不执行
     *
     * @param batchSize 批次大小
     * @param list      需分批的list
     * @param function  待执行方法
     * @param <R>       待执行方法的返回类型
     * @return
     */
    public static <O, R extends List, T> List<O> batchExeList(int batchSize, List<T> list, Function<List<T>, R> function) {
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }

        List<O> result = new ArrayList<>();
        int total = list.size();

        for (int i = 0; i < total; i += batchSize) {
            int nextIndex = i + batchSize;
            List<T> subList = list.subList(i, Math.min(total, nextIndex));
            R r = function.apply(subList);
            result.addAll(r);
        }

        return result;
    }


    /**
     * 异步分批次执行方法，list为空则不执行
     *
     * @param batchSize 批次大小
     * @param list      需分批的list
     * @param function  待执行方法
     * @param <R>       待执行方法的返回类型
     * @return
     */
    public static <R, T> List<R> batchExeAync(int batchSize, List<T> list, Function<List<T>, R> function) {
        if (CollectionUtils.isEmpty(list)) {
            return Lists.newArrayList();
        }

        List<R> result = new ArrayList<>();
        int total = list.size();

        for (int i = 0; i < total; i += batchSize) {
            int nextIndex = i + batchSize;
            List<T> subList = list.subList(i, Math.min(total, nextIndex));
            R r = function.apply(subList);
            result.add(r);
        }

        return result;
    }

}
