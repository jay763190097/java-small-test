package test_utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日期工具类
 *  <pre>
 *
 *	主要分为两大系列：
 *      format系列：日期转化成字符串，如formatYyyyMmDd()、formatYyyyMmDdSplit()等
 *      parse系列：字符串转化成日期，如parseYyyyMmDd()、parseYyyyMmDdSplit()等
 *
 *		方法名的命名规则：
 *          1.	yyyymmdd 	四位的年份+月份+日期，如20190714
 *          2.	yymmdd		两位的年份+月份+日期，如190714
 *          3.	hhmmss		时分秒
 *          4.	split		在日期中代表日期用“-”分隔，在时间中代表时间用“：”分隔
 *  </pre>
 *
 */
public class DateTimeFormatUtils {
	private static Logger logger = LoggerFactory.getLogger(DateTimeFormatUtils.class);

	private static final char CHAR_LINE = '-';
	private static final char CHAR_COLON = ':';
	private static final char CHAR_BLANK = ' ';

	private static final String TIME_FORMAT_HHMM = "hhmm";
	private static final String TIME_FORMAT_HHMM_SPLIT = "hh:mm";
	private static final String TIME_FORMAT_HHMMSS = "hhmmss";
	private static final String TIME_FORMAT_HHMMSS_SPLIT = "hh:mm:ss";

	private static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
	private static final String DATE_FORMAT_YYYYMMDD_SPLIT = "yyyy-MM-dd";
	private static final String DATE_FORMAT_YYMMDD = "yyMMdd";
	private static final String DATE_FORMAT_YYMMDD_SPLIT = "yy-MM-dd";
	private static final String DATE_FORMAT_MMDD = "MMdd";
	private static final String DATE_FORMAT_MMDD_SPLIT = "MM-dd";
	private static final String DATE_FORMAT_DD = "dd";

	/**
	 * 日期缓存中在今天之前的天数
	 */
	public static final int DATE_CACHE_BEFORE_NOW = 365;
	/**
	 * 日期缓存中在今天之后的天数
	 */
	public static final int DATE_CACHE_AFTER_NOW = 365;
	/**
	 * 日期缓存的总数
	 */
	public static final int DATE_CACHE_COUNT = DATE_CACHE_BEFORE_NOW + 1 + DATE_CACHE_AFTER_NOW;
	/**
	 * 日期缓存
	 */
	private static final DateInfo[] dateInfos = new DateInfo[DATE_CACHE_COUNT];
	/**
	 * 日期缓存中第一天的毫秒数
	 */
	private static long firstMillis;
	/**
	 * 时间缓存，一共86400个（24 * 60 * 60）
	 */
	private static final TimeInfo[] timeInfos = new TimeInfo[DateTimeUtils.SECOND_OF_DAY];

	/**
	 * 字符串对应日期缓存
	 */
	private static final Map<String, Date> yyyymmddDateMap = new HashMap<String, Date>();
	private static final Map<String, Date> yyyymmddSplitDateMap = new HashMap<String, Date>();
	private static final Map<String, Date> yymmddDateMap = new HashMap<String, Date>();
	private static final Map<String, Date> yymmddSplitDateMap = new HashMap<String, Date>();
	private static final Map<String, Date> mmddDateMap = new HashMap<String, Date>();
	private static final Map<String, Date> mmddSplitDateMap = new HashMap<String, Date>();

	/**
	 * 字符串对应时间缓存
	 */
	private static final Map<String, Integer> hhmmssMillisMap = new HashMap<String, Integer>();
	private static final Map<String, Integer> hhmmssSplitMillisMap = new HashMap<String, Integer>();
	private static final Map<String, Integer> hhmmMillisMap = new HashMap<String, Integer>();
	private static final Map<String, Integer> hhmmSplitMillisMap = new HashMap<String, Integer>();

	static {
		Date now = new Date();
		long nowTimeTruncated = DateTimeUtils.trunc(now);
		long millisInterval = DATE_CACHE_BEFORE_NOW * DateTimeUtils.MILLIS_OF_DAY;
		firstMillis = nowTimeTruncated - millisInterval;
		int nowYear = new DateTime(now).getYear();
		for (int i = 0; i < dateInfos.length; i++) {
			dateInfos[i] = buildDateInfo(new Date(firstMillis - DateTimeUtils.TIME_OFFSET + i * DateTimeUtils.MILLIS_OF_DAY), nowYear);
		}
		String firstDate = dateInfos[0].yyyymmddSplit;
		String lastDate = dateInfos[dateInfos.length - 1].yyyymmddSplit;
		logger.info("init date info cache finished, from {} to {}", firstDate, lastDate);

		for (int i = 0; i < timeInfos.length; i++) {
			timeInfos[i] = buildTimeInfo(i);
		}
	}

	private static DateInfo buildDateInfo(Date orgDate, int nowYear) {
		DateTime orgDateTime = new DateTime(orgDate.getTime());
		int year = orgDateTime.getYear();
		int month = orgDateTime.getMonthOfYear();
		int day = orgDateTime.getDayOfMonth();
		DateInfo dateInfo = new DateInfo(year, month, day);
		buildString2Date(orgDate, dateInfo, nowYear);
		return dateInfo;
	}

	private static void buildString2Date(Date orgDate, DateInfo dateInfo, int nowYear) {
		yyyymmddDateMap.put(dateInfo.yyyymmdd, orgDate);
		yyyymmddSplitDateMap.put(dateInfo.yyyymmddSplit, orgDate);
		yymmddDateMap.put(dateInfo.yymmdd, orgDate);
		yymmddSplitDateMap.put(dateInfo.yymmddSplit, orgDate);
		if (dateInfo.year == nowYear) {
			mmddDateMap.put(dateInfo.mmdd, orgDate);
			mmddSplitDateMap.put(dateInfo.mmddSplit, orgDate);
		}
	}

	private static TimeInfo buildTimeInfo(int secondOfDay) {
		int hour = secondOfDay / DateTimeUtils.SECOND_OF_HOUR;
		int secondsLtHour = secondOfDay % DateTimeUtils.SECOND_OF_HOUR;
		int minute = secondsLtHour / DateTimeUtils.SECOND_OF_MINUTE;
		int second = secondsLtHour % DateTimeUtils.SECOND_OF_MINUTE;
		TimeInfo timeInfo = new TimeInfo(hour, minute, second);
		Integer millisOfDayInteger = secondOfDay * DateTimeUtils.MILLIS_OF_SECOND;
		buildString2Millis(timeInfo, millisOfDayInteger);
		return timeInfo;
	}

	private static void buildString2Millis(TimeInfo timeInfo, Integer millisOfDayInteger) {
		hhmmssMillisMap.put(timeInfo.hhmmss, millisOfDayInteger);
		hhmmssSplitMillisMap.put(timeInfo.hhmmssSplit, millisOfDayInteger);
		if (!hhmmMillisMap.containsKey(timeInfo.hhmm)) {
			hhmmMillisMap.put(timeInfo.hhmm, millisOfDayInteger);
			hhmmSplitMillisMap.put(timeInfo.hhmmSplit, millisOfDayInteger);
		}
	}

	/**
	 * 格式化日期，包括年月日，例如：2014-12-24 10:20:30 >> 20141224
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatYyyyMmDd(Date date) {
		return format(date, DateFormatStyle.YYYYMMDD);
	}

	/**
	 * 格式化日期，，包括年月日，有分隔符，例如：2014-12-24 10:20:30 >> 2014-12-24
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatYyyyMmDdSplit(Date date) {
		return format(date, DateFormatStyle.YYYYMMDD_SPLIT);
	}

	/**
	 * 格式化日期，包括年（两位）月日，例如：2014-12-24 10:20:30 >> 141224
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatYyMmDd(Date date) {
		return format(date, DateFormatStyle.YYMMDD);
	}

	/**
	 * 格式化日期，包括年（两位）月日，有分隔符，例如：2014-12-24 10:20:30 >> 14-12-24
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatYyMmDdSplit(Date date) {
		return format(date, DateFormatStyle.YYMMDD_SPLIT);
	}

	/**
	 * 格式化日期，包括月日，例如：2014-12-24 10:20:30 >> 1224
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatMmDd(Date date) {
		return format(date, DateFormatStyle.MMDD);
	}
 
	/**
	 * 格式化日期，包括日，例如：2014-12-24 10:20:30 >> 24
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatDd(Date date) {
		return format(date, DateFormatStyle.DD);
	}
	
	/**
	 * 格式化日期，包括月日，有分隔符，例如：2014-12-24 10:20:30 >> 12-24
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatMmDdSplit(Date date) {
		return format(date, DateFormatStyle.MMDD_SPLIT);
	}

	static String format(Date date, DateFormatStyle dateFormatStyle) {
		if (date == null) {
			return null;
		}
		if (dateFormatStyle == null) {
			throw new IllegalArgumentException("format can't be null");
		}
		long truncSecond = DateTimeUtils.trunc(date);
		int interval = (int) ((truncSecond - firstMillis) / DateTimeUtils.MILLIS_OF_DAY);
		// 缓存中不存在该日期的信息
		if (interval < 0 || interval >= DATE_CACHE_COUNT) {
			return simpleFormat(date, dateFormatStyle.value);
		}
		DateInfo dateInfo = dateInfos[interval];
		switch (dateFormatStyle) {
			case YYYYMMDD:
				return dateInfo.yyyymmdd;
			case YYYYMMDD_SPLIT:
				return dateInfo.yyyymmddSplit;
			case YYMMDD:
				return dateInfo.yymmdd;
			case YYMMDD_SPLIT:
				return dateInfo.yymmddSplit;
			case MMDD:
				return dateInfo.mmdd;
			case MMDD_SPLIT:
				return dateInfo.mmddSplit;
			default:
				// 缓存中不存在该日期的这种格式
				return simpleFormat(date, dateFormatStyle.value);
		}
	}

	private static String simpleFormat(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * 格式化时间，包括时分，例如：2014-12-24 10:20:30 >> 1020
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatHhMm(Date date) {
		return format(date, TimeFormatStyle.HHMM);
	}

	/**
	 * 格式化时间，包括时分，有分隔符，例如：2014-12-24 10:20:30 >> 10:20
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatHhMmSplit(Date date) {
		return format(date, TimeFormatStyle.HHMM_SPLIT);
	}

	/**
	 * 格式化时间，包括时分秒，例如：2014-12-24 10:20:30 >> 102030
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatHhMmSs(Date date) {
		return format(date, TimeFormatStyle.HHMMSS);
	}

	/**
	 * 格式化时间，包括时分秒，有分隔符，例如：2014-12-24 10:20:30 >> 10:20:30
	 *
	 * @param date 需要格式化的日期
	 * @return
	 */
	public static String formatHhMmSsSplit(Date date) {
		return format(date, TimeFormatStyle.HHMMSS_SPLIT);
	}

	static String format(Date date, TimeFormatStyle timeFormatStyle) {
		if (date == null) {
			return null;
		}
		if (timeFormatStyle == null) {
			throw new IllegalArgumentException("timeFormatStyle can't be null");
		}
		int secondOfDay = (int) (((date.getTime() + DateTimeUtils.TIME_OFFSET) % DateTimeUtils.MILLIS_OF_DAY) / 1000);
		TimeInfo timeInfo = timeInfos[secondOfDay];
		switch (timeFormatStyle) {
			case HHMMSS:
				return timeInfo.hhmmss;
			case HHMMSS_SPLIT:
				return timeInfo.hhmmssSplit;
			case HHMM:
				return timeInfo.hhmm;
			case HHMM_SPLIT:
				return timeInfo.hhmmSplit;
			default:
				// 缓存中不存在该日期的这种格式
				return simpleFormat(date, timeFormatStyle.value);
		}
	}

	/**
	 * 格式化日期和时间，包括年月日（无分隔）时分秒（无分隔），例如：2014-12-24 10:20:30 >> 20141224 102030
	 *
	 * @param date  需要格式化的日期
	 * @param space 是否用空格间隔日期与时间
	 * @return
	 */
	public static String formatYyyyMmDdHhMmSs(Date date, boolean space) {
		return format(date, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMMSS, space);
	}

	/**
	 * 格式化日期和时间，包括年月日（无分隔）时分秒（分隔），例如：2014-12-24 10:20:30 >> 20141224 10:20:30
	 *
	 * @param date  需要格式化的日期
	 * @param space 是否用空格间隔日期与时间
	 * @return
	 */
	public static String formatYyyyMmDdHhMmSsSplit(Date date, boolean space) {
		return format(date, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMMSS_SPLIT, space);
	}

	/**
	 * 格式化日期和时间，包括年月日（分隔）时分（分隔），例如：2014-12-24 10:20:30 >> 2014-12-24 10:20
	 *
	 * @param date  需要格式化的日期
	 * @param space 是否用空格间隔日期与时间
	 * @return
	 */
	public static String formatYyyyMmDdSplitHhMmSplit(Date date, boolean space) {
		return format(date, DateFormatStyle.YYYYMMDD_SPLIT, TimeFormatStyle.HHMM_SPLIT, space);
	}
	
	/**
	 * 格式化日期和时间，包括年月日（分隔）时分（分隔），例如：2014-12-24 10:20:30 >> 2014-12-24 10:20
	 *
	 * @param date  需要格式化的日期
	 * @param space 是否用空格间隔日期与时间
	 * @return
	 */
	public static String formatYyyyMmDdHhMmSplit(Date date, boolean space) {
		return format(date, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMM, space);
	}
	
	
	/**
	 * 格式化日期和时间，包括年月日（分隔）时分（分隔），例如：2014-12-24 10:20:30 >> 201412241020
	 *
	 * @param date  需要格式化的日期
	 * @param space 是否用空格间隔日期与时间
	 * @author 01370598 
	 * @return
	 */
	public static String formatYyyyMmDdHhMm(Date date, boolean space) {
		return format(date, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMM, space);
	}
	
	/**
	 * 格式化日期和时间，包括年月日（分隔）时分（分隔），例如：2014-12-24 10:20:30 >> 12-24 10:20
	 *
	 * @param date  需要格式化的日期
	 * @param space 是否用空格间隔日期与时间
	 * @return
	 */
	public static String formatMmDdSplitHhMmSplit(Date date, boolean space) {
		return format(date, DateFormatStyle.MMDD_SPLIT, TimeFormatStyle.HHMM_SPLIT, space);
	}

	/**
	 * 格式化日期和时间，包括年月日（分隔）时分秒（分隔），例如：2014-12-24 10:20:30 >> 2014-12-24 10:20:30
	 *
	 * @param date  需要格式化的日期
	 * @param space 是否用空格间隔日期与时间
	 * @return
	 */
	public static String formatYyyyMmDdSplitHhMmSsSplit(Date date, boolean space) {
		return format(date, DateFormatStyle.YYYYMMDD_SPLIT, TimeFormatStyle.HHMMSS_SPLIT, space);
	}

	/**
	 * 格式化日期和时间，包括年月日时分秒，通过参数定制，实现不同的日期格式转换。
	 *
	 * @param date 需要格式化的日期
	 * @param dateFormatStyle 日期样式
	 * @param timeFormatStyle 时间样式
	 * @param space 是否用空格分隔日期与时间
	 * @return
	 */
	public static String format(Date date, DateFormatStyle dateFormatStyle, TimeFormatStyle timeFormatStyle, boolean space) {
		if (date == null) {
			return null;
		}
		if (dateFormatStyle == null) {
			throw new IllegalArgumentException("dateFormatStyle can't be null");
		}
		if (timeFormatStyle == null) {
			throw new IllegalArgumentException("timeFormatStyle can't be null");
		}
		StringBuilder sb = new StringBuilder();
		sb.append(format(date, dateFormatStyle));
		if (space) {
			sb.append(CHAR_BLANK);
		}
		sb.append(format(date, timeFormatStyle));
		return sb.toString();
	}

	private static void twoLength(StringBuilder sb, int amount) {
		if (amount < 10) {
			sb.append('0');
		}
		sb.append(amount);
	}

	/**
	 * 将字符串转化成日期，字符串的前几位类似"20141224"  >> 2014-12-24 00:00:00
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @return
	 */
	public static Date parseYyyyMmDd(String dateStr) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD);
	}

	/**
	 * 将字符串转化成日期，字符串的前几位类似"2014-12-24"  >> 2014-12-24 00:00:00
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @return
	 */
	public static Date parseYyyyMmDdSplit(String dateStr) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD_SPLIT);
	}

	static Date parse(String dateStr, DateFormatStyle dateFormatStyle) {
		if (dateStr == null) {
			return null;
		}
		if (dateFormatStyle == null) {
			throw new IllegalArgumentException("format can't be null");
		}
		Date result;
		String cutDateStr = cutDateStr(dateStr, dateFormatStyle);
		switch (dateFormatStyle) {
			case YYYYMMDD:
				result = yyyymmddDateMap.get(cutDateStr);
				break;
			case YYYYMMDD_SPLIT:
				result = yyyymmddSplitDateMap.get(cutDateStr);
				break;
			case YYMMDD:
				result = yymmddDateMap.get(cutDateStr);
				break;
			case YYMMDD_SPLIT:
				result = yymmddSplitDateMap.get(cutDateStr);
				break;
			case MMDD:
				result = mmddDateMap.get(cutDateStr);
				break;
			case MMDD_SPLIT:
				result = mmddSplitDateMap.get(cutDateStr);
				break;
			default:
				// 缓存中不存在该日期的这种格式
				return simpleParse(cutDateStr, dateFormatStyle.value);
		}
		// 日期在缓存中不在在
		if (result == null) {
			return simpleParse(cutDateStr, dateFormatStyle.value);
		}
		return result;
	}

	private static String cutDateStr(String dateStr, DateFormatStyle dateFormatStyle) {
		return dateStr.substring(0, dateFormatStyle.size);
	}

	private static String cutTimeStr(String dateStr, TimeFormatStyle timeFormatStyle) {
		return dateStr.substring(dateStr.length() - timeFormatStyle.size);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"2014-12-24"，后几位类似"102030" >> 2014-12-24 10:20:30
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdSplitHhMmSs(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD_SPLIT, TimeFormatStyle.HHMMSS, space);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"2014-12-24"，后几位类似"10:20:30" >> 2014-12-24 10:20:30
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdSplitHhMmSsSplit(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD_SPLIT, TimeFormatStyle.HHMMSS_SPLIT, space);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"2014-12-24"，后几位类似"1020" >> 2014-12-24 10:20:00
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdSplitHhMm(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD_SPLIT, TimeFormatStyle.HHMM, space);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"2014-12-24"，后几位类似"10:20" >> 2014-12-24 10:20:00
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdSplitHhMmSplit(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD_SPLIT, TimeFormatStyle.HHMM_SPLIT, space);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"20141224"，后几位类似"102030" >> 2014-12-24 10:20:30
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdHhMmSs(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMMSS, space);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"20141224"，后几位类似"10:20:30" >> 2014-12-24 10:20:30
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdHhMmSsSplit(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMMSS_SPLIT, space);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"20141224"，后几位类似"1020" >> 2014-12-24 10:20:00
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdHhMm(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMM, space);
	}

	/**
	 * 将字符串转化成时间，字符串前几位类似"20141224"，后几位类似"10:20" >> 2014-12-24 10:20:00
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param space 字符串日期与时间之间是否有空格
	 * @return
	 */
	public static Date parseYyyyMmDdHhMmSplit(String dateStr, boolean space) {
		return parse(dateStr, DateFormatStyle.YYYYMMDD, TimeFormatStyle.HHMM_SPLIT, space);
	}
	
	/**
	 * 10:20 -> 1970-01-01 10:20
	 * @param dateStr
	 * @return
	 */
	public static Date parseHhMmSplit(String dateStr) {
		try {
			return DateUtils.parseDate(dateStr, "HH:mm");
		} catch (ParseException e) {
			logger.error(" parseHhMmSplit.error:", e);
		}
		return null;
	}

	/**
	 * 将字符串转化成时间，通过参数定制，实现不同格式的转化。
	 *
	 * @param dateStr 需要转成日期的日期字符串
	 * @param dateFormatStyle 日期格式
	 * @param timeFormatStyle 时间格式
	 * @param spaceConnect 日期与时间是否与空格相连
	 * @return
	 */
	public static Date parse(String dateStr, DateFormatStyle dateFormatStyle, TimeFormatStyle timeFormatStyle, boolean spaceConnect) {
		if (dateStr == null) {
			return null;
		}
		if (dateFormatStyle == null) {
			throw new IllegalArgumentException("dateFormatStyle can't be null");
		}
		if (timeFormatStyle == null) {
			throw new IllegalArgumentException("timeFormatStyle can't be null");
		}
		Integer millis;
		String timeStr = cutTimeStr(dateStr, timeFormatStyle);
		switch (timeFormatStyle) {
			case HHMMSS:
				millis = hhmmssMillisMap.get(timeStr);
				break;
			case HHMMSS_SPLIT:
				millis = hhmmssSplitMillisMap.get(timeStr);
				break;
			case HHMM:
				millis = hhmmMillisMap.get(timeStr);
				break;
			case HHMM_SPLIT:
				millis = hhmmSplitMillisMap.get(timeStr);
				break;
			default:
				// 缓存中不存在该日期的这种格式
				return simpleParse(dateStr, dateFormatStyle, timeFormatStyle, spaceConnect);
		}
		// 在缓存中不在在
		if (millis == null) {
			throw new RuntimeException("wrong dateStr = " + dateStr);
		}
		Date date = parse(dateStr, dateFormatStyle);
		return new Date(date.getTime() + millis);
	}

	private static Date simpleParse(String dateStr, DateFormatStyle dateFormatStyle, TimeFormatStyle timeFormatStyle, boolean spaceConnect) {
		StringBuilder sb = new StringBuilder();
		sb.append(dateFormatStyle.value);
		if (spaceConnect) {
			sb.append(CHAR_BLANK);
		}
		sb.append(timeFormatStyle.value);
		return simpleParse(dateStr, sb.toString());
	}

	private static Date simpleParse(String dateStr, String format) {
		try {
			return new SimpleDateFormat(format).parse(dateStr);
		} catch (ParseException e) {
			throw new RuntimeException("dateStr = " + dateStr + " format = " + format, e);
		}
	}

	private static class DateInfo {
		private int year;
		private int month;
		private int day;
		private String yyyymmdd;
		private String yyyymmddSplit;
		private String yymmdd;
		private String yymmddSplit;
		private String mmdd;
		private String mmddSplit;

		public DateInfo(int year, int month, int day) {
			this.year = year;
			this.month = month;
			this.day = day;
			init();
		}

		private void init() {
			StringBuilder sb = new StringBuilder();
			sb.append(year);
			twoLength(sb, month);
			twoLength(sb, day);
			yyyymmdd = sb.toString();

			sb = new StringBuilder();
			sb.append(year).append(CHAR_LINE);
			twoLength(sb, month);
			sb.append(CHAR_LINE);
			twoLength(sb, day);
			yyyymmddSplit = sb.toString();

			sb = new StringBuilder();
			sb.append(getYearOfTwoDigit());
			twoLength(sb, month);
			twoLength(sb, day);
			yymmdd = sb.toString();

			sb = new StringBuilder();
			sb.append(getYearOfTwoDigit()).append(CHAR_LINE);
			twoLength(sb, month);
			sb.append(CHAR_LINE);
			twoLength(sb, day);
			yymmddSplit = sb.toString();

			sb = new StringBuilder();
			twoLength(sb, month);
			twoLength(sb, day);
			mmdd = sb.toString();

			sb = new StringBuilder();
			twoLength(sb, month);
			sb.append(CHAR_LINE);
			twoLength(sb, day);
			mmddSplit = sb.toString();
		}

		private int getYearOfTwoDigit() {
			return year % 100;
		}
	}

	private static class TimeInfo {
		private int hour;
		private int minute;
		private int second;
		private String hhmmss;
		private String hhmmssSplit;
		private String hhmm;
		private String hhmmSplit;

		public TimeInfo(int hour, int minute, int second) {
			this.hour = hour;
			this.minute = minute;
			this.second = second;
			init();
		}

		private void init() {
			StringBuilder sb = new StringBuilder();
			twoLength(sb, hour);
			twoLength(sb, minute);
			twoLength(sb, second);
			hhmmss = sb.toString();

			sb = new StringBuilder();
			twoLength(sb, hour);
			sb.append(CHAR_COLON);
			twoLength(sb, minute);
			sb.append(CHAR_COLON);
			twoLength(sb, second);
			hhmmssSplit = sb.toString();

			sb = new StringBuilder();
			twoLength(sb, hour);
			twoLength(sb, minute);
			hhmm = sb.toString();

			sb = new StringBuilder();
			twoLength(sb, hour);
			sb.append(CHAR_COLON);
			twoLength(sb, minute);
			hhmmSplit = sb.toString();
		}
	}

	/**
	 * 日期格式
	 */
	public enum DateFormatStyle {
		/**
		 * 年月日，如：20140712
		 */
		YYYYMMDD(DATE_FORMAT_YYYYMMDD, DATE_FORMAT_YYYYMMDD.length()),
		/**
		 * 年月日，有分隔符， 如：2014-07-12
		 */
		YYYYMMDD_SPLIT(DATE_FORMAT_YYYYMMDD_SPLIT, DATE_FORMAT_YYYYMMDD_SPLIT.length()),
		/**
		 * 年（两位）月日，如：140712
		 */
		YYMMDD(DATE_FORMAT_YYMMDD, DATE_FORMAT_YYMMDD.length()),
		/**
		 * 年（两位）月日，有分隔符， 如：14-07-12
		 */
		YYMMDD_SPLIT(DATE_FORMAT_YYMMDD_SPLIT, DATE_FORMAT_YYMMDD_SPLIT.length()),
		/**
		 * 月日，如：0712
		 */
		MMDD(DATE_FORMAT_MMDD, DATE_FORMAT_MMDD.length()),
		/**
		 * 月日，有分隔符，如：07-12
		 */
		MMDD_SPLIT(DATE_FORMAT_MMDD_SPLIT, DATE_FORMAT_MMDD_SPLIT.length()),
		/**
		 * 日，有分隔符，如：12
		 */
		DD(DATE_FORMAT_DD, DATE_FORMAT_DD.length())
		;

		private final String value;
		private final int size;

		DateFormatStyle(String value, int size) {
			this.value = value;
			this.size = size;

		}

	}

	/**
	 * 时间格式
	 */
	public enum TimeFormatStyle {
		/**
		 * 时分秒，如：132450
		 */
		HHMMSS(TIME_FORMAT_HHMMSS, TIME_FORMAT_HHMMSS.length()),
		/**
		 * 时分秒，有分隔符，如：13:24:50
		 */
		HHMMSS_SPLIT(TIME_FORMAT_HHMMSS_SPLIT, TIME_FORMAT_HHMMSS_SPLIT.length()),
		/**
		 * 时分，如：1324
		 */
		HHMM(TIME_FORMAT_HHMM, TIME_FORMAT_HHMM.length()),
		/**
		 * 时分，有分隔符，如：13:24
		 */
		HHMM_SPLIT(TIME_FORMAT_HHMM_SPLIT, TIME_FORMAT_HHMM_SPLIT.length());

		private final String value;
		private final int size;

		TimeFormatStyle(String value, int size) {
			this.value = value;
			this.size = size;
		}
	}
	
//	public static void main(String[] args) {
//		System.out.println(formatYyyyMmDdHhMmSplit(new Date(), false));
//	}
}
