package test_earyrules;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.RuleListener;

public class HelloWorldRuleListener implements RuleListener {
	
	// 评估之前
	@Override
	public boolean beforeEvaluate(Rule rule, Facts facts) {
		System.out.println("============beforeEvaluate===========");
		return true;
	}
	
	// 评估之后，可以得到评估的结果
	@Override
	public void afterEvaluate(Rule rule, Facts facts, boolean evaluationResult) {
		System.out.println("============afterEvaluate==========="+rule.getName()+evaluationResult);
		if(!evaluationResult) {
			System.out.println(rule.getDescription());
		}
	}
	
	// 评估出错
	@Override
	public void onEvaluationError(Rule rule, Facts facts, Exception exception) {
		System.out.println("++++++++++++++++++++");
		System.out.println(exception.getMessage());
	}

	// 执行失败
	@Override
	public void onFailure(Rule rule, Facts facts, Exception exception) {
		System.out.println("=========onFailure==========");
		System.out.println(rule.getName()+"fail");
	}
	
	// 执行成功
	@Override
	public void onSuccess(Rule rule, Facts facts) {
		System.out.println("========onSuccess===========");
	}
	
	
	
}
