package test_earyrules;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Rule;

@Rule(priority = 1, name = "Hello World rule2", description = "Always say hello world error")
public class HelloWorldRule2 {

	@Condition
    public boolean when() {
		return false;
		//throw new RuntimeException("rule2 error");
        //return false;
    }
 
    @Action
    public void then() throws Exception {
        System.out.println("hello world2");
    }
	
}
