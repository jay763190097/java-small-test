package test_earyrules;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Rule;

@Rule(priority = 2, name = "Hello World rule", description = "Always say hello world")
public class HelloWorldRule {

	@Condition
    public boolean when() {
        return true;
    }
 
    @Action
    public void then() throws Exception {
        System.out.println("excute hello world");
        throw new RuntimeException("rule error");
    }
	
}
