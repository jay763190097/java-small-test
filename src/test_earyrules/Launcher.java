package test_earyrules;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.api.RulesEngineParameters;
import org.jeasy.rules.core.AbstractRulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.InferenceRulesEngine;

public class Launcher {

	public static void main(String[] args) {
		// create facts
        Facts facts = new Facts();
 
        // create rules
        Rules rules = new Rules();
        rules.register(new HelloWorldRule());
        rules.register(new HelloWorldRule2());
 
        // create a rules engine and fire rules on known facts
        RulesEngineParameters parameters = new RulesEngineParameters()
        	    .priorityThreshold(10)
        	    .skipOnFirstAppliedRule(false)
        	    .skipOnFirstFailedRule(true)
        	    .skipOnFirstNonTriggeredRule(false);
        AbstractRulesEngine rulesEngine = new DefaultRulesEngine(parameters);
        rulesEngine.registerRuleListener(new HelloWorldRuleListener());
        rulesEngine.fire(rules, facts);
	}

}
