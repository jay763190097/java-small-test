package test_jaxb;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Created by stpass-wlm on 2017-08-01.
 */
public class HandoverList implements Serializable {
    private String storeOutId;
    private LocalDateTime deliverTime;
    private String handoverId;
    private String description;
    private int handoverType;
    private int bags;
    private String bloodInfo;
    private String prepareId;
    private String deliverBranch;
    private String deliverer;

    public HandoverList() {
    }

    public String getStoreOutId() {
        return storeOutId;
    }

    public void setStoreOutId(String storeOutId) {
        this.storeOutId = storeOutId;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(LocalDateTime deliverTime) {
        this.deliverTime = deliverTime;
    }

    public String getHandoverId() {
        return handoverId;
    }

    public void setHandoverId(String handoverId) {
        this.handoverId = handoverId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHandoverType() {
        return handoverType;
    }

    public void setHandoverType(int handoverType) {
        this.handoverType = handoverType;
    }

    public int getBags() {
        return bags;
    }

    public void setBags(int bags) {
        this.bags = bags;
    }

    public String getBloodInfo() {
        return bloodInfo;
    }

    public void setBloodInfo(String bloodInfo) {
        this.bloodInfo = bloodInfo;
    }

    public String getPrepareId() {
        return prepareId;
    }

    public void setPrepareId(String prepareId) {
        this.prepareId = prepareId;
    }

    public String getDeliverBranch() {
        return deliverBranch;
    }

    public void setDeliverBranch(String deliverBranch) {
        this.deliverBranch = deliverBranch;
    }

    public String getDeliverer() {
        return deliverer;
    }

    public void setDeliverer(String deliverer) {
        this.deliverer = deliverer;
    }
}
