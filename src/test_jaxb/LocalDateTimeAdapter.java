package test_jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by bennu on 2017/7/5.
 */
public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {
	
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    @Override
    public LocalDateTime unmarshal(String v) throws Exception {
        if(isNullOrEmpty(v))
            return null;
        return LocalDateTime.parse(v, formatter);
    }

    @Override
    public String marshal(LocalDateTime v) throws Exception {
        if( v != null)
            return v.format(formatter);
        return null;
    }
    
    public boolean isNullOrEmpty(String string) {
    	return string == null || "".equals(string.trim());
    }
}
