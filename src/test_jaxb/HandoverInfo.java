package test_jaxb;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Administrator on 2017/06/30.
 */
@XmlRootElement
public class HandoverInfo implements Serializable {
    private static final long serialVersionUID = -4644547L;

    private String id;//唯一编号
    private String deliverId;//交付方业务单号
    private String deliverer;//交付人
    private LocalDateTime deliverTime;//交付时间
    private int source;//发送方
    private int destination;//接收方
    private int type;//单据类型
    private LocalDateTime acceptTime;//接收时间
    private String deliverBranch;//交付机构
    private String remark;//备注
    private List<HandoverDetailInfo> bloodBags;//血袋
    private List<HandoverSumInfo> sums;//统计信息

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeliverId() {
        return deliverId;
    }

    public void setDeliverId(String deliverId) {
        this.deliverId = deliverId;
    }

    public String getDeliverer() {
        return deliverer;
    }

    public void setDeliverer(String deliverer) {
        this.deliverer = deliverer;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(LocalDateTime deliverTime) {
        this.deliverTime = deliverTime;
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    public LocalDateTime getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(LocalDateTime acceptTime) {
        this.acceptTime = acceptTime;
    }

    public String getDeliverBranch() {
        return deliverBranch;
    }

    public void setDeliverBranch(String deliverBranch) {
        this.deliverBranch = deliverBranch;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<HandoverDetailInfo> getBloodBags() {
        return bloodBags;
    }

    public void setBloodBags(List<HandoverDetailInfo> bloodBags) {
        this.bloodBags = bloodBags;
    }

    public List<HandoverSumInfo> getSums() {
        return sums;
    }

    public void setSums(List<HandoverSumInfo> sums) {
        this.sums = sums;
    }
}
