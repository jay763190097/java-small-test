package test_jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by Administrator on 2017/06/30.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class HandoverDetailInfo implements Serializable {
    private static final long serialVersionUID = -13541L;

    private String bloodBagId;
    private String serialId;
    private String bloodId;
    private String bloodGroup;
    private String rhBloodGroup;
    private int collector;
    private int producer;
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime collectTime;
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime produceTime;
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    private LocalDateTime expiration;
    private String branch;
    private boolean status = false;

    public HandoverDetailInfo() {

    }

    public String getBloodBagId() {
        return bloodBagId;
    }

    public void setBloodBagId(String bloodBagId) {
        this.bloodBagId = bloodBagId;
    }

    public String getSerialId() {
        return serialId;
    }

    public void setSerialId(String serialId) {
        this.serialId = serialId;
    }

    public String getBloodId() {
        return bloodId;
    }

    public void setBloodId(String bloodId) {
        this.bloodId = bloodId;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getRhBloodGroup() {
        return rhBloodGroup;
    }

    public void setRhBloodGroup(String rhBloodGroup) {
        this.rhBloodGroup = rhBloodGroup;
    }

    public int getCollector() {
        return collector;
    }

    public void setCollector(int collector) {
        this.collector = collector;
    }

    public int getProducer() {
        return producer;
    }

    public void setProducer(int producer) {
        this.producer = producer;
    }

    public LocalDateTime getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(LocalDateTime collectTime) {
        this.collectTime = collectTime;
    }

    public LocalDateTime getProduceTime() {
        return produceTime;
    }

    public void setProduceTime(LocalDateTime produceTime) {
        this.produceTime = produceTime;
    }

    public LocalDateTime getExpiration() {
        return expiration;
    }

    public void setExpiration(LocalDateTime expiration) {
        this.expiration = expiration;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
