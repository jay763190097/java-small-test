package test_jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

/**
 * Created by Administrator on 2017/06/30.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class HandoverSumInfo implements Serializable {
    private static final long serialVersionUID = -4647L;
    private String bloodSubType;
    private String bloodGroup;
    private String rhBloodGroup;
    private Double conversion;
    private Double capacity;

    public HandoverSumInfo() {

    }

    public String getBloodSubType() {
        return bloodSubType;
    }

    public void setBloodSubType(String bloodSubType) {
        this.bloodSubType = bloodSubType;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getRhBloodGroup() {
        return rhBloodGroup;
    }

    public void setRhBloodGroup(String rhBloodGroup) {
        this.rhBloodGroup = rhBloodGroup;
    }

    public Double getConversion() {
        return conversion;
    }

    public void setConversion(Double conversion) {
        this.conversion = conversion;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }
}
