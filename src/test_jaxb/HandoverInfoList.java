package test_jaxb;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created by stpass-wlm on 2017-08-01.
 */
@XmlRootElement
public class HandoverInfoList implements Serializable {
	
    private List<HandoverList> handoverList;

    public HandoverInfoList() {
    }

    public List<HandoverList> getHandoverList() {
        return handoverList;
    }

    public void setHandoverList(List<HandoverList> handoverList) {
        this.handoverList = handoverList;
    }
}
