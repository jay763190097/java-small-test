package test_itext_drawExcelTable;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

public class MainItext7 {
	
	public static final String DEST2 = "E:\\test4.pdf";// 文件路径

	public static void main(String[] args) throws Exception {
		test(DEST2);
	}

	public static void test(String dest) throws Exception {
		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
		Document doc = new Document(pdfDoc);
		PdfFont sysFont = PdfFontFactory.createFont("STSongStd-Light",
				"UniGB-UCS2-H", false);
		doc.setFont(sysFont);
		doc.setFontSize(6);
        //创建总表形式（一行四格）
		float[] columns = new float[] { 20, 30, 22, 22, 22, 30, 30, 30 };
		Table table = new Table(columns).setWidth(512);
		//表头
		for(int i=0; i < columns.length; i++){
			table.addCell(new Cell().add(new Paragraph(""+(i+1))).setWidth(columns[i]));
		}
		//表格行合并"2"代表合并2行单元格
		
		Cell cell=new Cell(1,columns.length)
				.setTextAlignment(TextAlignment.CENTER)
				.add(new Paragraph("  东风汽车维修厂   维修工单"));
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("订单编号"))
				.setBold()
				.setBorderRight(Border.NO_BORDER)
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,3)
				.add(new Paragraph("OD4616516"))
				.setBorderLeft(Border.NO_BORDER)
				.setBorderRight(Border.NO_BORDER);
		table.addCell(cell);
		cell=new Cell(1,3)
				.add(new Paragraph("单位：  元    "))
				.setTextAlignment(TextAlignment.RIGHT)
				.setBorderLeft(Border.NO_BORDER);
		table.addCell(cell);
		
		
		/* ------------------------基本信息----------------------- */
		cell=new Cell(1,2)
				.add(new Paragraph("客户名称 "))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,3)
				.add(new Paragraph("顺丰 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("车牌号"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("粤B1234"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("车型 "))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,3)
				.add(new Paragraph("依维柯"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("维修类型"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("定期维护"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("进场时间"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,3)
				.add(new Paragraph("2018/08/08"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("出厂时间"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("2018/09/08"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("联系人"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,3)
				.add(new Paragraph("346849"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("联系电话"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.setWidth(50)
				.add(new Paragraph("17688886666"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		
		
		
		/* ------------------------工时----------------------- */
		cell=new Cell()
				.add(new Paragraph("工时编码 "))
				.setWidth(30)
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("工时名称 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("工时数量 "))
				.setWidth(25)
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("工时单价 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("优惠金额 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1, 3)
				.add(new Paragraph("小计 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		
		
		/* ------------------------配件----------------------- */
		cell=new Cell()
				.add(new Paragraph("配件编码 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("配件名称 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("配件数量 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("单位 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("配件单价 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("优惠金额 "))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("小计"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		
		
		cell=new Cell()
				.add(new Paragraph("总金额"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("壹仟叁佰伍拾圆(￥1350元)"))
				.setWidth(70)
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("优惠金额"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("伍拾圆(￥50元)"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("结算金额"))
				.setBold()
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		cell=new Cell(1,2)
				.add(new Paragraph("壹仟叁佰圆(￥1300元)"))
				.setTextAlignment(TextAlignment.CENTER);
		table.addCell(cell);
		
		
		cell=new Cell(1,2)
				.add(new Paragraph("操作者账号"))
				.setTextAlignment(TextAlignment.CENTER)
				.setBorderRight(Border.NO_BORDER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("346849"))
				.setTextAlignment(TextAlignment.CENTER)
				.setBorderLeft(Border.NO_BORDER)
				.setBorderRight(Border.NO_BORDER);
		table.addCell(cell);
		cell=new Cell(1,3)
				.add(new Paragraph(""))
				.setBorderLeft(Border.NO_BORDER)
				.setBorderRight(Border.NO_BORDER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph("验收签字"))
				.setBorderLeft(Border.NO_BORDER)
				.setBorderRight(Border.NO_BORDER);
		table.addCell(cell);
		cell=new Cell()
				.add(new Paragraph(""))
				.setBorderLeft(Border.NO_BORDER);
		table.addCell(cell);
		
		doc.add(table);
		doc.close();
	}
}
