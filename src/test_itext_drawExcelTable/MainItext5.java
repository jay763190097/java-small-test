package test_itext_drawExcelTable;

import java.io.FileOutputStream;
import java.io.IOException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class MainItext5 {

	public static final String DEST2 = "E:\\test3.pdf";// 文件路径

	public static void main(String[] args) throws Exception {
		createPdf(DEST2);
	}
	
	
	public static void createPdf(String fileName) throws DocumentException, IOException {
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(DEST2));
		document.open();
		PdfPTable table = createTable();
		document.add(table);
		document.close();
	}
	
	
    public static PdfPTable createTable() throws DocumentException, IOException {
    	BaseFont bfChinese = BaseFont.createFont( "STSongStd-Light" ,"UniGB-UCS2-H",false );  
    	Font font =  new Font(bfChinese,7,Font.NORMAL);
    	Font blodFont =  new Font(bfChinese,7,Font.BOLD);
    	float[] columns = new float[] { 25, 45, 22, 22, 22, 25, 25, 30 };
        PdfPTable table = new PdfPTable(columns.length);
       // table.setWidthPercentage(288 / 5.23f);
        table.setWidths(columns);
        table.setTotalWidth(512);
        table.setLockedWidth(true);
        PdfPCell cell;
        
        cell = new PdfPCell(new Phrase("  东风汽车维修厂   维修工单", font));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(8);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("订单编号", font));
        cell.setColspan(2);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthRight(0);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("OD14616516234", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("单位：  元      ", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
        
		
		cell=new PdfPCell(new Phrase("客户名称 ", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("顺丰速运 ", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("车牌号 ", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("粤B1234 ", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("车型 ", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("依维柯 ", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("维修类型 ", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("定期维护 ", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("进场时间", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("2018/08/08 ", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("出厂时间 ", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("2018/09/08 ", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("联系人", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("346849 ", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("联系电话 ", blodFont));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("17688886666", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
        
		/* ------------------------工时----------------------- */
		cell=new PdfPCell(new Phrase("工时编码", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("工时名称", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("工时数量", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("工时单价", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("优惠金额", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("小计", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		
		/* ------------------------配件----------------------- */
		cell=new PdfPCell(new Phrase("配件编码", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("配件名称", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("配件数量", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("单位", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("配件单价", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("优惠金额", font));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("小计", font));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setBorderWidthLeft(0);
		table.addCell(cell);
		
		
		cell=new PdfPCell(new Phrase("总金额", blodFont));
		cell.setBorderWidthTop(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("壹仟叁佰伍拾圆(￥1350元)", font));
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("优惠金额", blodFont));
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("伍拾圆(￥50元)", font));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("结算金额", blodFont));
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("壹仟叁佰圆(￥1300元)", font));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		
		
		cell=new PdfPCell(new Phrase("操作者账号", font));
		cell.setColspan(2);
		cell.setBorderWidthTop(0);
		cell.setBorderWidthRight(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("346849", font));
		cell.setBorderWidthTop(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("", font));
		cell.setColspan(3);
		cell.setBorderWidthTop(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("验收签字", font));
		cell.setBorderWidthTop(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		cell=new PdfPCell(new Phrase("", font));
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
		
        return table;
    }
}
