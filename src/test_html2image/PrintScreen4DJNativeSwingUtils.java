package test_html2image;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import chrriis.dj.nativeswing.swtimpl.NativeComponent;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserEvent;

public class PrintScreen4DJNativeSwingUtils extends JPanel{
	
	private static final long serialVersionUID = 1L;  
    // 行分隔符  
    final static public String LS = System.getProperty("line.separator", "/n");  
    // 文件分割符  
    final static public String FS = System.getProperty("file.separator", "//");  
    // 当网页超出目标大小时  截取
    final static public int maxWidth = 2000;  
    final static public int maxHeight = 1400;  

    /**  
     * @param file  预生成的图片全路径
     * @param url   网页地址
     * @param width  打开网页宽度 ，0 = 全屏
     * @param height 打开网页高度 ，0 = 全屏
     * @return  boolean
     * @author AndyBao
     * @version V4.0, 2016年9月28日 下午3:55:52 
     */
    public  PrintScreen4DJNativeSwingUtils(final String file,final String url,final String WithResult){
          super(new BorderLayout());  
            JPanel webBrowserPanel = new JPanel(new BorderLayout());  
            final JWebBrowser webBrowser = new JWebBrowser(null);  
            webBrowser.setBarsVisible(false);  
            webBrowser.navigate(url);  
            webBrowserPanel.add(webBrowser, BorderLayout.CENTER);  
            add(webBrowserPanel, BorderLayout.CENTER);  
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 4));  
            webBrowser.addWebBrowserListener(new WebBrowserAdapter() {  
                // 监听加载进度  
                public void loadingProgressChanged(WebBrowserEvent e) {  
                    // 当加载完毕时  
                    if (e.getWebBrowser().getLoadingProgress() == 100) {  
                        String result = (String) webBrowser.executeJavascriptWithResult(WithResult);  
                        int index = result == null ? -1 : result.indexOf(":");  
                        NativeComponent nativeComponent = webBrowser  
                                .getNativeComponent();  
                        Dimension originalSize = nativeComponent.getSize();  
                        Dimension imageSize = new Dimension(Integer.parseInt(result  
                                .substring(0, index)), Integer.parseInt(result  
                                .substring(index + 1)));  
                        imageSize.width = Math.max(originalSize.width,  
                                imageSize.width + 50);  
                        imageSize.height = Math.max(originalSize.height,  
                                imageSize.height + 50);  
                        nativeComponent.setSize(imageSize);  
                        BufferedImage image = new BufferedImage(imageSize.width,  
                                imageSize.height, BufferedImage.TYPE_INT_RGB);  
                        nativeComponent.paintComponent(image);  
                        nativeComponent.setSize(originalSize);  
                        // 当网页超出目标大小时  
                        if (imageSize.width > maxWidth  
                                || imageSize.height > maxHeight) {  
                            //截图部分图形  
                            image = image.getSubimage(0, 0, maxWidth, maxHeight);  
                          //  此部分为使用缩略图 
                           /* int width = image.getWidth(), height = image 
                                .getHeight(); 
                             AffineTransform tx = new AffineTransform(); 
                            tx.scale((double) maxWidth / width, (double) maxHeight 
                                    / height); 
                            AffineTransformOp op = new AffineTransformOp(tx, 
                                    AffineTransformOp.TYPE_NEAREST_NEIGHBOR); 
                            //缩小 
                            image = op.filter(image, null);  */
                        }  
                        try {  
                            // 输出图像  
                            ImageIO.write(image, "jpg", new File(file));  
                        } catch (IOException ex) {  
                            ex.printStackTrace();  
                        }  
                        // 退出操作  
                        System.exit(0);  
                    }  
                }  
            }  
            );  
            add(panel, BorderLayout.SOUTH);  

    }

     //以javascript脚本获得网页全屏后大小  
    public static String getScreenWidthHeight(){

            StringBuffer  jsDimension = new StringBuffer();  
            jsDimension.append("var width = 0;").append(LS);  
            jsDimension.append("var height = 0;").append(LS);  
            jsDimension.append("if(document.documentElement) {").append(LS);  
            jsDimension.append(  
                            "  width = Math.max(width, document.documentElement.scrollWidth);")  
                    .append(LS);  
            jsDimension.append(  
                            "  height = Math.max(height, document.documentElement.scrollHeight);")  
                    .append(LS);  
            jsDimension.append("}").append(LS);  
            jsDimension.append("if(self.innerWidth) {").append(LS);  
            jsDimension.append("  width = Math.max(width, self.innerWidth);")  
                    .append(LS);  
            jsDimension.append("  height = Math.max(height, self.innerHeight);")  
                    .append(LS);  
            jsDimension.append("}").append(LS);  
            jsDimension.append("if(document.body.scrollWidth) {").append(LS);  
            jsDimension.append(  
                    "  width = Math.max(width, document.body.scrollWidth);")  
                    .append(LS);  
            jsDimension.append(  
                    "  height = Math.max(height, document.body.scrollHeight);")  
                    .append(LS);  
            jsDimension.append("}").append(LS);  
            jsDimension.append("return width + ':' + height;");  

           return jsDimension.toString();
    }

    public static boolean printUrlScreen2jpg(final String file,final String url,final int width,final int height){

         NativeInterface.open();  
            SwingUtilities.invokeLater(new Runnable() {  
                public void run() {  
                    String withResult="var width = "+width+";var height = "+height+";return width +':' + height;";
                    if(width==0||height==0)
                        withResult=getScreenWidthHeight();

                    // SWT组件转Swing组件，不初始化父窗体将无法启动webBrowser  
                    JFrame frame = new JFrame("网页截图");  
                    // 加载指定页面，最大保存为640x480的截图  
                    frame.getContentPane().add(  
                            new PrintScreen4DJNativeSwingUtils(file,url, withResult),  
                            BorderLayout.CENTER);  
                    frame.setSize(640, 480);  
                    // 仅初始化，但不显示  
                    frame.invalidate();  

                    frame.pack();  
                    frame.setVisible(false);  
                }  
            });  
            NativeInterface.runEventPump();  

        return true;
    }

    public static void main(String[] args) {  

        PrintScreen4DJNativeSwingUtils.printUrlScreen2jpg("1123.jpg", "<div class=\"\\&quot;Voucher-detail\\&quot;\" style=\"\\&quot;padding-top:5px\\&quot;\">							\r\n" + 
        		"        		\"<table cellpadding=\"\\&quot;1\\&quot;\" cellspacing=\"\\&quot;1\\&quot;\" style=\"\\&quot;background-color:\" #c7c7c7;=\"\" width:100%;\\\"=\"\">								<tbody><tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" rowspan=\"\\&quot;1\\&quot;\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">订单号：</td>									<td style=\"\\&quot;line-height:25px;\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\" colspan=\"\\&quot;2\\&quot;\">										14070909400808201990									</td>								</tr>																<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" rowspan=\"\\&quot;1\\&quot;\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">订单状态</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\" colspan=\"\\&quot;2\\&quot;\">																																												处理成功																																																																																														</td>								</tr>																<tr>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118;\\\"=\"\" rowspan=\"\\&quot;5\\&quot;\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">付款人信息</td>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118;\\\"=\"\">汇款人姓名</td>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:400\\\"=\"\">奥巴马0009(GF)</td>								</tr>								<tr>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118;\\\"=\"\">支付账号</td>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:400\\\"=\"\">121212121212</td>								</tr>								<tr>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118;\\\"=\"\">汇款币种金额</td>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:400\\\"=\"\">USD 12.0</td>								</tr>								<tr>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118;\\\"=\"\">支付银行及卡号</td>									<td style=\"\\&quot;\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:400\\\"=\"\">中国银行；121212121212</td>								</tr>								<tr>									<td style=\"\\&quot;line-height:25px;\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">汇款人地址</td>									<td style=\"\\&quot;\" line-height:25px;=\"\" background-color:=\"\" white;width:400\\\"=\"\">如果彼得德鲁克说管理是种实践是对的，那管理的灵魂就必然是一种独立思考的精神，因为唯有独立思考才能完成打穿理论与现实，完成特殊到一般，一般再到特殊这样的轮回。</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" rowspan=\"\\&quot;9\\&quot;\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">收款人信息</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">收款人名称</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">布兰2</td>									</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">开户银行名称</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">守夜人银行</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">开户银行SWIFT编号</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">122334</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">开户银行ABA编号</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\"></td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">开户银行IBAN编号</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\"></td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">收款人帐号</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">622512349876</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">收款人地址</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">临冬城</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">收款人代理银行信息</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">																					代理行名称地址：WebResizer:在线图片压缩工具提供了一种更便捷的在线压缩服务，图片大小减少了不少。但是图片效果依然是那么好，另外还可以在图片上添加边...；																															代理行开户号：33；																																									代理行SWIFT Code：11																			</td>								</tr>																								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">常驻国家名称及代码</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">美国/110</td>								</tr>																<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" rowspan=\"\\&quot;5\\&quot;\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">服务明细</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">购汇金额</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">USD: 12.0</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">购汇汇率</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">5.988</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">等值人民币</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">71.86</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">手续费</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">人民币0.72</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\">电报费</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;\\\"=\"\">人民币150.0</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">交易附言</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;;\\\"=\"\" colspan=\"\\&quot;2\\&quot;\">管理就是这样的一种东西，每个人都可以说上几句，但你很难识别这是对的还是错的，很难识别它究竟对公司的成功是一种正向的助推力还是一种逆向的杀伤力</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">汇款附言</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;;\\\"=\"\" colspan=\"\\&quot;2\\&quot;\">管理就是这样的一种东西，每个人都可以说上几句，但你很难识别这是对的还</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">手续费扣款方式</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;;\\\"=\"\" colspan=\"\\&quot;2\\&quot;\" \\\"=\"\\&quot;\\&quot;\">																																												付款方支付手续费																														</td>								</tr>								<tr>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;width:118px;\\\"=\"\" align=\"\\&quot;center\\&quot;\" valign=\"\\&quot;middle\\&quot;\">合计</td>									<td style=\"\\&quot;\" word-break:break-all;=\"\" word-wrap:break-word;=\"\" line-height:25px;=\"\" padding-left:5px;=\"\" background-color:=\"\" white;;\\\"=\"\" colspan=\"\\&quot;2\\&quot;\">人民币										222.58									</td>								</tr>								</tbody></table>						</div>", 375, 667);
    }  
	
}
