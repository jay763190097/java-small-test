package test_optional;

import java.math.BigDecimal;
import java.util.Optional;

import test_object_extends.Sun1;

public class Test {

	public static void main(String[] args) {
	/*	System.out.println(Optional
				.ofNullable("****8")
				.map(str -> str.substring(0, 2))
				.orElse("字符串为空"));*/
		
		Sun1 sun1 = new Sun1();
		//sun1.setName("abc");
		System.out.println(Optional.ofNullable(Optional
				.ofNullable(sun1)
				.map(str -> str.getPrice())
				.orElse(null)).map(str -> str.toString()).orElse(""));
		
	}
	
}
