package test_base64_img;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Target;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Test {

	/**
	 * 示例
	 */
	public static void main(String[] args) {
	    String strImg = getImageStr("F:/262208938330189287.jpg");
	    System.out.println("==================\n"+strImg);
	    generateImage(strImg, "F:/tp.jpg");
	}
	
	
	/**
	 * @Description: 根据图片地址转换为base64编码字符串
	 * @Author: 
	 * @CreateTime: 
	 * @return 返回字符串
	 */
	@Deprecated
	public static String getImageStr(String imgFile) {
	    InputStream inputStream = null;
	    byte[] data = null;
	    try {
	        inputStream = new FileInputStream(imgFile);
	        data = new byte[inputStream.available()];
	        inputStream.read(data);
	        inputStream.close();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    // 加密
	    BASE64Encoder encoder = new BASE64Encoder();
	    return encoder.encode(data);
	}
	
	
	
	/**
	 * @Description: 将base64编码字符串转换为图片
	 * @Author: 
	 * @CreateTime: 
	 * @param imgStr base64编码字符串
	 * @param path 图片路径-具体到文件
	 * @return 返回
	*/
	public static boolean generateImage(String imgStr, String path) {
			if (imgStr == null) {
				return false;
			}
			BASE64Decoder decoder = new BASE64Decoder();
			try {
				//解密
				byte[] b = decoder.decodeBuffer(imgStr);
				//处理数据
				for (int i = 0; i < b.length; ++i) {
					if (b[i] < 0) {
						b[i] += 256;
					}
				}
				OutputStream out = new FileOutputStream(path);
				out.write(b);
				out.flush();
				out.close();
				return true;
			} catch (Exception e) {
				// TODO: handle exception
				return false;
			}
	}
	
}
