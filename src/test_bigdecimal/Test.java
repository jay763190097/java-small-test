package test_bigdecimal;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Test {

	public static void main(String[] args) {
		
		BigDecimal balance = new BigDecimal("100.25");
		BigInteger calorie = new BigInteger("235");
		BigDecimal consume = new BigDecimal("20.75");
		
		BigDecimal add = consume
				.divide(new BigDecimal("40"),BigDecimal.ROUND_HALF_DOWN)
				.multiply(new BigDecimal("238"))
				.setScale(0,BigDecimal.ROUND_DOWN);
		
		BigDecimal rt = balance.subtract(consume);
		
		System.out.println(add);
		System.out.println(rt);
		
		
		/**
		 * double 和 float 精度丢失的问题，需要精确小数点的，都需要采用BigDecimal
		 */
		
		/*double d = 1099.9;
		float f = 1099.9f;
		
		System.out.println(d * 100);
		System.out.println(f + 2.01f);
		System.out.println(0.11+2001299.32);*/
		
		
		BigDecimal totalBig = new BigDecimal(10);
		BigDecimal tardyBig = new BigDecimal(5);
		System.out.println(totalBig.subtract(tardyBig).divide(totalBig, 4, BigDecimal.ROUND_HALF_UP));
		
		BigDecimal decimal = new BigDecimal(-10);
		System.out.println(decimal.doubleValue() < 0d);
	}
	
}
