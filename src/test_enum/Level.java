package test_enum;

public enum Level {

	上级医师("11"),
    主任医师("12"),
    医务科("13");    //每一项相当于调用一个构造函数

    private String value;

    private Level(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
