package test_regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	
	
	public static void main(String[] args) {
		
		String sql = "select st.*,cursor(select job,sum(salary) as \"sum\" from group_test group by job) as \"salary\" from student st";
		sql = sql.replaceAll("cursor\\(.*\\)", "' '");
		System.out.println(sql);
		
		System.out.println(new String[]{"xxx"}[0]);
		
		Pattern pattern1 = Pattern.compile("(?<=\\{)[^\\}]+");
		String value = "custom: {name 名称 String,age 年龄 int}";
		Matcher m = pattern1.matcher(value);
        while (m.find()) {
            System.out.println("Found value: " + m.group() );
        }
        
        String aString = "safa<asfdsaf>";
        // 会报错，所以substring的时候需要判断一下长度
        System.out.println(aString.substring(0,100));
        
	}

}
